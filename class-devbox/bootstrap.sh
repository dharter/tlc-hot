apt update
apt -y dist-upgrade
apt autoremvoe

# vmtunnel service, add ports for ssh, xrdp and guacamole http

# gnome desktop
apt install -y ubuntu-desktop
apt instlal -y xrdp

# xrdp server
cat > /home/ubuntu/.xsessionrc <<EOF
echo export GNOME_SHELL_SESSION_MODE=ubuntu
export XDG_CURRENT_DESKTOP=ubuntu:GNOME
export XDG_CONFIG_DIRS=/etc/xdg/xdg-ubuntu:/etc/xdg
EOF

systemctl enable xrdp
systemctl start xrdp


# guacamole desktop server
# reference: https://www.linuxbabe.com/ubuntu/apache-guacamole-remote-desktop-ubuntu-20-04
apt install -y build-essential libcairo2-dev libjpeg-turbo8-dev libpng-dev libtool-bin libossp-uuid-dev libvncserver-dev freerdp2-dev libssh2-1-dev libtelnet-dev libwebsockets-dev libpulse-dev libvorbis-dev libwebp-dev libssl-dev libpango1.0-dev libswscale-dev libavcodec-dev libavutil-dev libavformat-dev

# get guac source, extract, build, install
wget http://mirror.cc.columbia.edu/pub/software/apache/guacamole/1.2.0/source/guacamole-server-1.2.0.tar.gz
tar xvfz guacamole-server-1.2.0.tar.gz
cd guacamole-server-1.2.0

./configure --with-init-dir=/etc/init.d --enable-guacenc
make
make install
ldconfig

# start and enable the guacamole server
systemctl daemon-reload
systemctl start guacd
systemctl enable guacd
# systemctl status guacd # check system service enabled correctly
# ss -lnpt | grep guacd # check server running on port 4822 as expected

# install guacamole web application, served from Apache tomcat using Java Servlets
apt install -y tomcat9 tomcat9-admin tomcat9-common tomcat9-user
# ss -lnpt | grep java # check apache tomcat now running port 8080

# get the guacample web app
wget https://downloads.apache.org/guacamole/1.2.0/binary/guacamole-1.2.0.war
mv guacamole-1.2.0.war /var/lib/tomcat9/webapps/guacamole.war
systemctl restart tomcat9 guacd

# configure guacample
mkdir /etc/guacamole

cat <<EOF > /etc/guacamole/guacamole.properties
# hostname and port of guacamole proxy
guacd-hostname: localhost
guacd-port:     4822

# Auth provider class (authenticates user/pass combination, needed if using the provided login screen)
basic-user-mapping:   /etc/guacamole/user-mapping.xml
auth-provider:  net.sourceforge.guacamole.net.basic.BasicFileAuthenticationProvider
EOF

# create desktop connections file
cat <<EOF > /etc/guacamole/user-mapping.xml
<user-mapping>
        
    <!-- Per-user authentication and config information -->

    <!-- A user using md5 to hash the password
         guacadmin user and its md5 hashed password below is used to 
             login to Guacamole Web UI-->
    <authorize 
            username="dharter"
            password="Tbiwtts4U">

        <!-- Allow direct connection to xrdp server at port 3389 remote connection -->
        <connection name="Class DevBox Remote Desktop Protocol (RDP) Desktop Session">
            <protocol>rdp</protocol>
            <param name="hostname">127.0.0.1</param>
            <param name="port">3389</param>
            <param name="username">dharter</param>
            <param name="password">Tbiwtts4U</param>
            <param name="ignore-cert">true</param>
            <param name="enable-sftp">true</param>
            <param name="sftp-username">dharter</param>
            <param name="sftp-private-key">-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAzfGeOd0Iq4a3yK6+PrE7yV0ESxf28J5cmxOh2ojn2kmeBprW
OJU1Mf//GRqZBohaoAhMUwE1X7BcDOd1+B9dz6U46Hhk0/vPXUJhLi8Cun+zX9s8
6fE3e84+DSaIGrBxV5kjPc9/sV8UrnWBiY+50t4wbdl8pkWxrLlyH+1pPO8jUvN3
0FBbrCVAVu7I0J58Pa8JVBlW8qlksBtTS3d3ulmU0AEymu7MmLd9Xsh/tHodeepq
DGrZGh9YMxNboay++aRW1voW4Q9MWc6Fj8oBcGD+DzhD9wZXd0UHCmG7yT/J+Usr
0fUZKk/mFr3mkwdCADsQqM6n9zLYmcWKeN8/7wIDAQABAoIBABMNalTyFO5BbVk8
iU8leGF9oHsOAYNrMygJneyow8JXEy6LPoxHiX9Rrpj08/k9vhyWB/GZ6AUtPKbY
9vn4dOZT47rmAHX4EswO6wypvrfgPoj8sFr4POjQQL9YtDi74BB5k2ARhT+0iyZE
qrI+QLO4aISRHU4InpJbKaq4fQbs8jvxT/+/W3bmDiRZkF2oByTQWWBq61rJPaY4
6w9F3rUClpny2dVpjpq65xmsyRmkt+iazPCo9JyK6wqcu4oP/gqvnYjAyM4J6UWb
8ExBwwu2EmkBE+iZ3sJGwvNg0urQecNDokiVyK7hLNQgwiG0sTEQy+Nua/vmLiDV
ZyfFQdECgYEA6B61Juddagnf63eyeNYUSFDSuLGxBBEK+UOacCifRNJAuO9vINxR
CsPQbJKdPaHOQWWD0KhlSa74SFT1PeRb3fJnESEcH4EKynfJfIVf29eLHgT/5ikG
jOpJvoKO4OW622vLU47MqVkUupGd/5j69CGW6PnpcgdGXcwzasu3KUkCgYEA4yGD
8dzbq3147T0nFgn6GmMF2syIrSrPFbDhm+W1R/BcQ4NHuSYhnrCRsn0MG1YGcpeZ
1dXGmALWr7PnY2qplfgC3j9qzLyq5Xc6XgcpMdJhgKd7QHkY8Ozu5v0hKrTC7vxn
P2eYBY4vvWOIRHCIvZpTC8OSWdsO+ZlcP0tZl3cCgYEAvG9pWlrK3/GCQXKbhTMt
pgNO/bbq4EVBpld2QYUIvuL8veVKTt/sPpP2WJ2xXr5DZZ2Wo8aA649KadXmfUcs
OQK3HxXE8eJdMz6JaDgSOLNDcaxXXa6CbdUo5qqweOymg8h/eKbdiMETbd4W6M91
Mskt7JYtKG+1dSH3v8P/PlkCgYBtOKG1fepYpgavtreD/po5dCLNm4uiEjLxEFT9
XdH13RzRDmFAJJkIMYpulXlr78s78bD1Rk6wrLTGWzHK+fDBADmI3GAkQhnALLxx
mOnzXmKqtl8rmtAmkxkzFMiNO819lPylXdCZ400LusA9gQwQeRS0VICdGx0xY9Eb
UsWrcwKBgQCPGfkD4fgyHU9j+Y5rJTs9x07KaA9n4R6fUqZcAvcBKXMbEGSVKhzT
DhFwc5YLCC9Dxf76l9I8qfXTveOAYXD36AVkR7vfMxcQXLBp3ZG8IxpWVXWCH6K3
1xRhO9W9+HgJVdmRxWRXyoDcXGNmq3jYnfNkejJukEX3r02ZLuiXNw==
-----END RSA PRIVATE KEY-----</param>
            <param name="sftp-root-directory">\/home\/dharter</param>
        </connection>

    </authorize>

    <authorize 
            username="cturner"
            password="rooSai5a">

        <!-- Allow direct connection to xrdp server at port 3389 remote connection -->
        <connection name="Class DevBox Remote Desktop Protocol (RDP) Desktop Session">
            <protocol>rdp</protocol>
            <param name="hostname">127.0.0.1</param>
            <param name="port">3389</param>
            <param name="username">cturner</param>
            <param name="password">rooSai5a</param>
            <param name="ignore-cert">true</param>
            <param name="enable-sftp">true</param>
            <param name="sftp-username">cturner</param>
            <param name="sftp-private-key">-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAzfGeOd0Iq4a3yK6+PrE7yV0ESxf28J5cmxOh2ojn2kmeBprW
OJU1Mf//GRqZBohaoAhMUwE1X7BcDOd1+B9dz6U46Hhk0/vPXUJhLi8Cun+zX9s8
6fE3e84+DSaIGrBxV5kjPc9/sV8UrnWBiY+50t4wbdl8pkWxrLlyH+1pPO8jUvN3
0FBbrCVAVu7I0J58Pa8JVBlW8qlksBtTS3d3ulmU0AEymu7MmLd9Xsh/tHodeepq
DGrZGh9YMxNboay++aRW1voW4Q9MWc6Fj8oBcGD+DzhD9wZXd0UHCmG7yT/J+Usr
0fUZKk/mFr3mkwdCADsQqM6n9zLYmcWKeN8/7wIDAQABAoIBABMNalTyFO5BbVk8
iU8leGF9oHsOAYNrMygJneyow8JXEy6LPoxHiX9Rrpj08/k9vhyWB/GZ6AUtPKbY
9vn4dOZT47rmAHX4EswO6wypvrfgPoj8sFr4POjQQL9YtDi74BB5k2ARhT+0iyZE
qrI+QLO4aISRHU4InpJbKaq4fQbs8jvxT/+/W3bmDiRZkF2oByTQWWBq61rJPaY4
6w9F3rUClpny2dVpjpq65xmsyRmkt+iazPCo9JyK6wqcu4oP/gqvnYjAyM4J6UWb
8ExBwwu2EmkBE+iZ3sJGwvNg0urQecNDokiVyK7hLNQgwiG0sTEQy+Nua/vmLiDV
ZyfFQdECgYEA6B61Juddagnf63eyeNYUSFDSuLGxBBEK+UOacCifRNJAuO9vINxR
CsPQbJKdPaHOQWWD0KhlSa74SFT1PeRb3fJnESEcH4EKynfJfIVf29eLHgT/5ikG
jOpJvoKO4OW622vLU47MqVkUupGd/5j69CGW6PnpcgdGXcwzasu3KUkCgYEA4yGD
8dzbq3147T0nFgn6GmMF2syIrSrPFbDhm+W1R/BcQ4NHuSYhnrCRsn0MG1YGcpeZ
1dXGmALWr7PnY2qplfgC3j9qzLyq5Xc6XgcpMdJhgKd7QHkY8Ozu5v0hKrTC7vxn
P2eYBY4vvWOIRHCIvZpTC8OSWdsO+ZlcP0tZl3cCgYEAvG9pWlrK3/GCQXKbhTMt
pgNO/bbq4EVBpld2QYUIvuL8veVKTt/sPpP2WJ2xXr5DZZ2Wo8aA649KadXmfUcs
OQK3HxXE8eJdMz6JaDgSOLNDcaxXXa6CbdUo5qqweOymg8h/eKbdiMETbd4W6M91
Mskt7JYtKG+1dSH3v8P/PlkCgYBtOKG1fepYpgavtreD/po5dCLNm4uiEjLxEFT9
XdH13RzRDmFAJJkIMYpulXlr78s78bD1Rk6wrLTGWzHK+fDBADmI3GAkQhnALLxx
mOnzXmKqtl8rmtAmkxkzFMiNO819lPylXdCZ400LusA9gQwQeRS0VICdGx0xY9Eb
UsWrcwKBgQCPGfkD4fgyHU9j+Y5rJTs9x07KaA9n4R6fUqZcAvcBKXMbEGSVKhzT
DhFwc5YLCC9Dxf76l9I8qfXTveOAYXD36AVkR7vfMxcQXLBp3ZG8IxpWVXWCH6K3
1xRhO9W9+HgJVdmRxWRXyoDcXGNmq3jYnfNkejJukEX3r02ZLuiXNw==
-----END RSA PRIVATE KEY-----</param>
            <param name="sftp-root-directory">\/home\/cturner</param>
        </connection>

    </authorize>

</user-mapping>
EOF

systemctl restart tomcat9 guacd

# set up a reverse proxy for guacamole, allows serviing at port 80 and easier TLS certificates
# try nginx
apt install nginx

cat > /etc/nginx/conf.d/guacamole.conf <<"EOF" 
server {
        listen 80;
        listen [::]:80;
        server_name tlc.harter.pro;

        access_log  /var/log/nginx/guac_access.log;
        error_log  /var/log/nginx/guac_error.log;

        location / {
                    proxy_pass http://127.0.0.1:8080/guacamole/;
                    proxy_buffering off;
                    proxy_http_version 1.1;
                    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                    proxy_set_header Upgrade $http_upgrade;
                    proxy_set_header Connection $http_connection;
                    proxy_cookie_path /guacamole/ /;
        }

}
EOF

nginx -t # test nfinx configuration
systemctl reload nginx

# set up development tools and ide
apt -y install build-essential csh wget htop sshfs python dos2unix git
apt install -y uncrustify doxygen graphviz texlive-base texlive-latex-extra texlive-latex-recommended

# vim
apt install -y vim

# emacs
apt install -y emacs 

# atom
snap install atom --classic
sed -i "s|/snap/bin/atom ATOM_DISABLE_SHELLING_OUT_FOR_ENVIRONMENT=false /usr/bin/atom %F|/snap/bin/atom %F|g" /var/lib/snapd/desktop/applications/atom_atom.desktop

# sublime
snap install sublime-text --classic

# vs code
snap install code --classic

# following is specific for user
# clone class repository
export user=apant

#if need to create the user first
useradd -m -d /home/${user} -s /bin/bash ${user}
# passwd ${user}
# set up ssh keys if needed
su - ${user} -c "ssh-keygen -t ed25519 -f ~/.ssh/id_ed25519"
cat /home/${user}/.ssh/id_ed25519.pub > /home/${user}/.ssh/authorized_keys
chown ${user}:${user} /home/${user}/.ssh/authorized_keys
chmod go-rwx /home/${user}/.ssh/authorized_keys

cp /home/ubuntu/.xsessionrc /home/${user}/.xsessionrc
chown ${user}:${user} /home/${user}/.xsessionrc

mkdir -p /home/${user}/repos
chown -R ${user}:${user} /home/${user}/repos
su - ${user} -c "git clone https://bitbucket.org/dharter/cosc2336-ds-alg repos/cosc2336-ds-alg"
cp /home/${user}/repos/cosc2336-ds-alg/scripts/emacs-init.el  /home/${user}/.emacs
chown ${user}:${user} /home/${user}/.emacs

su - ${user} -c "apm install intentions linter linter-gcc linter-ui-default atom-beautify busy-signal build build-make output-panel dbg dbg-gdb"

cat <<EOF > /home/${user}/.atom/config.cson
"*":
  "atom-beautify":
    cpp:
      beautify_on_save: true
      configPath: "/home/${user}/repos/cosc2336-ds-alg/config/uncrustify.cfg"
  build:
    saveOnBuild: true
  "build-make":
    useMake: true
  core:
    telemetryConsent: "no"
  editor:
    showIndentGuide: true
    softWrapAtPreferredLineLength: true
    tabType: "soft"
  "exception-reporting":
    userId: "ed566a28-a209-40a9-b7c7-a0027afcb0b3"
  "linter-gcc":
    gccIncludePaths: "/home/${user}/repos/cosc2336-ds-alg/include"
  welcome:
    showOnStartup: false
EOF
chown ${user}:${user} /home/${user}/.atom/config.cson

su - ${user} -c "code --install-extension laurenttreguier.uncrustify"
su - ${user} -c "code --install-extension ms-vscode.cpptools"
mkdir -p /home/${user}/.config/Code/User
cp /home/${user}/repos/cosc2336-ds-alg/.vscode/keybindings.json /home/${user}/.config/Code/User/keybindings.json
chown ${user}:${user} /home/${user}/.config/Code/User/keybindings.json
