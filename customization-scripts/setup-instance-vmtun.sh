#!/bin/bash
#
# Set up vmtun tunneling services during instance creation.

# create the bootstrap script
conf="/root/ubuntu-server-tunconf.sh"
cat > ${conf} <<SETUP_EOF
# copy the appropriate tunconf bootstrap here
SETUP_EOF

# execute it to install vmtun service and enable it
chmod u+x ${conf}
${conf}
