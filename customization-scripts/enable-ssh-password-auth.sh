#!/bin/bash
#
# Simple customization script.  If we don't have good network/ssh access to created instances, can only
# reliable access through horizon console, the default behavior of cloud images will be problematic.  Most
# clould images disable any password based authentication and allow auth only through public/private
# key with an injected key.  This customiztion script turns back on password authentication and
# sets a password for the default ubuntu user (change user/password for different os as needed)

echo "Allowing password authentication via ssh..."
sed -i 's/^PasswordAuthentication no$/PasswordAuthentication yes/g' /etc/ssh/sshd_config
#systemctl restart ssh

echo "Change password for default user..."
echo -e "ubuntu\nubuntu" | passwd ubuntu > /dev/null 2>&1
