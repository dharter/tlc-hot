#!/bin/bash
#
# Install Gnome GUIi on Centos 7: https://www.itzgeek.com/how-tos/linux/centos-how-tos/install-gnome-gui-on-centos-7-rhel-7.html
# https://draculaservers.com/tutorials/install-xrdp-centos/
#
# Having lots of problems getting xrdp server to work.  The following finally seemed to fix it:
# https://www.nakivo.com/blog/how-to-use-remote-desktop-connection-ubuntu-linux-walkthrough/

# use group to install, could install Mate or Cinnamon instead
# yum group list
yum groupinstall -y "GNOME Desktop" "Server with GUI"
yum install -y "xorg*"

# get xrdp installed and configured correctly according to the instructions
yum install -y xrdp

# the following should be added to the general copy user configuration
echo "xfce4-session" > /home/gaussian/.xsession
chown gaussian:gaussian /home/gaussian/.xsession

# fix the startwm.sh startup script
config="/usr/libexec/xrdp/startwm.sh"
cp $config $config.orig
cat > ${config} <<EOF
#!/bin/sh

if [ -r /etc/default/locale ]; then
  . /etc/default/locale
  export LANG LANGUAGE
fi

startxfce4

EOF

# fix the /etc/xrdp/xrdp.ini file

systemctl enable xrdp
systemctl start xrdp

# Need to add/start vm tunnel services at this point
