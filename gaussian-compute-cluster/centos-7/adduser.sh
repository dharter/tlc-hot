# create user and add them to the gaussian group
newuser="llatta"
useradd -m -d /home/${newuser} -s /bin/bash ${newuser}
usermod -a -G gaussian ${newuser}

# set up ssh keys
mkdir -p /home/${newuser}/.ssh
chown ${newuser}:${newuser} -R /home/${newuser}/.ssh
chmod 700 /home/${newuser}/.ssh

su -s /bin/bash -c "ssh-keygen -t rsa -b 2048 -P '' -f /home/${newuser}/.ssh/id_rsa -C 'gaussian' " ${newuser}
su -s /bin/bash -c "cat /home/${newuser}/.ssh/id_rsa.pub >> /home/${newuser}/.ssh/authorized_keys" ${newuser}

cat /home/centos/.ssh/authorized_keys | grep "Nova" >> /home/${newuser}/.ssh/authorized_keys
cat /root/.ssh/authorized_keys | grep "Ansible" >> /home/${newuser}/.ssh/authorized_keys


# copy reference and desktop files for the initial user desktop setup
mkdir -p /home/${newuser}/Desktop
cp /home/gaussian/Desktop/* /home/${newuser}/Desktop
cp /home/gaussian/.xsession /home/${newuser}
chown -R ${newuser}:${newuser} /home/${newuser}

mkdir -p /home/scratch/${newuser}
chown -R ${newuser}:gaussian /home/scratch/${newuser}

# run the copy cluster script to copy user info to all compute nodes and update their
# .bashrc
ansible-playbook compute-cluster-copylocal-playbook.yml

# edit the /etc/guacamole/user-map.xml file and add the user to give guacamole access
# password: aivu4Voh
    <authorize 
            username="langel"
            password="aivu4Voh">


        <!-- Allow direct connection to ssh command line terminal -->
        <connection name="Gaussian SSH Terminal Access">
            <protocol>ssh</protocol>
            <param name="hostname">127.0.0.1</param>
            <param name="port">22</param>
            <param name="server-alive-interval">600</param>
            <param name="username">langel</param>
            <param name="private-key">-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEA1lItmDq7KJcBdjbMe+Y73YEH7QfpRALiYhh1RiZTuNXvxgup
onFh8xTjuhYOLFim80zhDA5Vv6P9unoekvE2qBGtwyYdBhdGajnoPdqCDs15BkCg
0G7DaIzodnLaDxsKd0/dQnAekkO2YluVk7+qUKPgKScMLo+sXUyBgTUfp4/U/8Oy
sjPlTw204PzPmv1QePkCZpDjLzQpS/ank2dIW2XPIkxn2XCdX+VGTkXXBdcWvjms
LV3TQnIl/Zqyt12/DtmxJBhgeCiExftYAxl5PhdWPrwUO6qgytkFpsksZV3PIu/q
Gc44sv8aOjCiB9e/65XpcUKfJ7JovmNmua0U0wIDAQABAoIBAF+QCPCFz4oLuwUg
7hCY2jysGgKgsmw29QZUbYY39MDH87/U13CtKcWfnOMOjodLiDWYJAiiI63ty2bo
qNVQZNSP/XC0f+CFZEpdQ6bA1ZkQGVSmYI5PtSXbHpQiJgTyQ7pri/bh01/YfWxh
9dIJdkG6sGrpFE92lJ6NTHl2XITzllcR+3hY96REadeGj8BaY2/RjVYGJTEnl0ZB
6igiuKdqeieGi7vV0v3vnv1iuxwBu2kBN6fbrO0lnAFK8931oxSiKnoF9Cxzv1RI
wk0Ymy3NowoU5DYeE7TOiRJMYkxOVVLZEufeBtQX4UYT9ID1Az8KtWgSDk5xagT7
1bWM1ZECgYEA7auQXexUFXt0ZrDrkt0AfI59bjFb1qaiixZaSpuMIXCUuD0hZ82F
MN4jUoagQ0xprtPueevhXEBdKmcf6o0L+orkQALUCIR+BoMVy6RjLybV65qNTxs1
t0VrEyQ80J264pjdwMNkiNNNPbMjJSzN77e7f4vE5b950VUDN0ZrQSUCgYEA5tme
1eZxKqCYN9lVqc15mNVKhwSpM+1KWyzbfOQeotwkSHrPIFWicQMKQOYQ1ihc/mof
LXsMdqaHoWGuKTTWHM04iaze8pJNXup3mT/MB9cWGN+Ccq5x2Pk90WxEKqUP1opC
m/yt7OYH+6C10ygDNMFe/dHfS6+asA9siBg2iJcCgYAhufremS7FbaNy/yAu0xXC
lyXNgUubBkKCokVl2PTlueggzOv0owGHkb5Jc7lCU5GbsY9h1ctBIBEYd1HcIu/U
Oelx+/TC0KphmjB5mXZJtqUcre3nj+DrhPpQ2Q+YOwnOJgsx04hssi9/1e/i0pzW
q6f7zYW/ZJCQ2BOAwIlv0QKBgFv7QCeYf2TtwlkWduSGo7m62sw7ljPDi8oScI4t
0usvbiGXFWqb1HLVNEOxp+m9AXi5zhxwGAI6znYGFsACdoHZX3qz782Vb9Cu+eKN
If3JvNtCS5boXSKIBRKp4etgA5SHWI0QvDhVW77uRSSSn9xoZDIcaq2clbUrICKG
C0yXAoGBAI0VvbLZag+EcRQV59VVlOf5xvQw6u0cZHJ8ucO5aGsn6A3MCt8Swox7
5DnYKkEbHhehtRZC1uPD9GEz1aheffQf3Z4Gi0kT7346mTpR3geVU/mAumoYIKhH
A9e0PB0c9cF8PoZySjSvx7bdftZsnFNjk6ziCvpBJusYEkwPHWko
-----END RSA PRIVATE KEY-----</param>
            <param name="enable-sftp">true</param>
            <param name="sftp-root-directory">\/home\/langel</param>
        </connection>

        <!-- Allow direct connection to xrdp server at port 3389 remote connection -->
        <connection name="Gaussian Remote Desktop Protocol (RDP) Desktop Session">
            <protocol>rdp</protocol>
            <param name="hostname">127.0.0.1</param>
            <param name="port">3389</param>
	    <param name="username">langel</param>
	    <param name="password">aivu4Voh</param>
            <param name="ignore-cert">true</param>
            <param name="enable-sftp">true</param>
            <param name="sftp-username">langel</param>
            <param name="sftp-private-key">-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEA1lItmDq7KJcBdjbMe+Y73YEH7QfpRALiYhh1RiZTuNXvxgup
onFh8xTjuhYOLFim80zhDA5Vv6P9unoekvE2qBGtwyYdBhdGajnoPdqCDs15BkCg
0G7DaIzodnLaDxsKd0/dQnAekkO2YluVk7+qUKPgKScMLo+sXUyBgTUfp4/U/8Oy
sjPlTw204PzPmv1QePkCZpDjLzQpS/ank2dIW2XPIkxn2XCdX+VGTkXXBdcWvjms
LV3TQnIl/Zqyt12/DtmxJBhgeCiExftYAxl5PhdWPrwUO6qgytkFpsksZV3PIu/q
Gc44sv8aOjCiB9e/65XpcUKfJ7JovmNmua0U0wIDAQABAoIBAF+QCPCFz4oLuwUg
7hCY2jysGgKgsmw29QZUbYY39MDH87/U13CtKcWfnOMOjodLiDWYJAiiI63ty2bo
qNVQZNSP/XC0f+CFZEpdQ6bA1ZkQGVSmYI5PtSXbHpQiJgTyQ7pri/bh01/YfWxh
9dIJdkG6sGrpFE92lJ6NTHl2XITzllcR+3hY96REadeGj8BaY2/RjVYGJTEnl0ZB
6igiuKdqeieGi7vV0v3vnv1iuxwBu2kBN6fbrO0lnAFK8931oxSiKnoF9Cxzv1RI
wk0Ymy3NowoU5DYeE7TOiRJMYkxOVVLZEufeBtQX4UYT9ID1Az8KtWgSDk5xagT7
1bWM1ZECgYEA7auQXexUFXt0ZrDrkt0AfI59bjFb1qaiixZaSpuMIXCUuD0hZ82F
MN4jUoagQ0xprtPueevhXEBdKmcf6o0L+orkQALUCIR+BoMVy6RjLybV65qNTxs1
t0VrEyQ80J264pjdwMNkiNNNPbMjJSzN77e7f4vE5b950VUDN0ZrQSUCgYEA5tme
1eZxKqCYN9lVqc15mNVKhwSpM+1KWyzbfOQeotwkSHrPIFWicQMKQOYQ1ihc/mof
LXsMdqaHoWGuKTTWHM04iaze8pJNXup3mT/MB9cWGN+Ccq5x2Pk90WxEKqUP1opC
m/yt7OYH+6C10ygDNMFe/dHfS6+asA9siBg2iJcCgYAhufremS7FbaNy/yAu0xXC
lyXNgUubBkKCokVl2PTlueggzOv0owGHkb5Jc7lCU5GbsY9h1ctBIBEYd1HcIu/U
Oelx+/TC0KphmjB5mXZJtqUcre3nj+DrhPpQ2Q+YOwnOJgsx04hssi9/1e/i0pzW
q6f7zYW/ZJCQ2BOAwIlv0QKBgFv7QCeYf2TtwlkWduSGo7m62sw7ljPDi8oScI4t
0usvbiGXFWqb1HLVNEOxp+m9AXi5zhxwGAI6znYGFsACdoHZX3qz782Vb9Cu+eKN
If3JvNtCS5boXSKIBRKp4etgA5SHWI0QvDhVW77uRSSSn9xoZDIcaq2clbUrICKG
C0yXAoGBAI0VvbLZag+EcRQV59VVlOf5xvQw6u0cZHJ8ucO5aGsn6A3MCt8Swox7
5DnYKkEbHhehtRZC1uPD9GEz1aheffQf3Z4Gi0kT7346mTpR3geVU/mAumoYIKhH
A9e0PB0c9cF8PoZySjSvx7bdftZsnFNjk6ziCvpBJusYEkwPHWko
-----END RSA PRIVATE KEY-----</param>
            <param name="sftp-root-directory">\/home\/langel</param>
        </connection>

    </authorize>
