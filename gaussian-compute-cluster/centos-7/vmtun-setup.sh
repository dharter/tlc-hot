#!/bin/bash
# General configuration to set up tunnels for services for a newly created
# vm instance/server in TheLionCloud openstack
#
# This script tested on Ubuntu server 18.04 and 19.04 versions and relies
# on presence of newer systemd services and that the ssh service is on
# and running by default.

# configure these parameters, use 0 for port number to not set up
# that tunnel type on configured server
tunnel_ip='45.33.118.94'
ssh_port='0'
http_port='12085'
https_port='0'
vnc_port='0'
rdp_port='12084'
jupyterhub_port='0'
rstudio_port='0'
my_netdev='eth0'

# install needed packages
echo "Installing autossh packages..."
yum install -y autossh


# create vmtun identification key so we can communicate with tunnel
# server machine to create tunnels
echo "Creating tunnel identity files..."
cat > /root/.ssh/vmtun.pem <<EOF
-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAAAMwAAAAtzc2gtZW
QyNTUxOQAAACCJu0kbNSXWR6L6j0q4zF+D95SQubRAKkgi6vATg61mSQAAAJiF0dnXhdHZ
1wAAAAtzc2gtZWQyNTUxOQAAACCJu0kbNSXWR6L6j0q4zF+D95SQubRAKkgi6vATg61mSQ
AAAEBdlsV3D89TBFm0ZgmIGcXjnCw0mhPwD4apE2WvucP3E4m7SRs1JdZHovqPSrjMX4P3
lJC5tEAqSCLq8BODrWZJAAAAFHJvb3RAbGlvbi1jb250cm9sbGVyAQ==
-----END OPENSSH PRIVATE KEY-----
EOF

# ssh won't allow identity files with too permissive permissions
chmod go-rwx /root/.ssh/vmtun.pem


# create the systemd service script
# NOTE: the quote aroudn "EOF" is important here, it suppresses variable expansion
# in the here document that follows, which is what we need here.
echo "Creating tunnel systemd services..."
cat > /usr/local/bin/vmtun.sh <<"EOF"
#!/bin/bash 
### BEGIN INIT INFO
# Provides:       vmtun
# Required-Start: $all
# Required-Stop:
# Default-Start:  2 3 4 5
# Default-Stop:
# Short-Description: set up ssh bridge to lion server
### END INIT INFO

# The network device and ip addresses we need for the tunnel
EOF

cat >> /usr/local/bin/vmtun.sh <<EOF
my_netdev='${my_netdev}'
EOF

cat >> /usr/local/bin/vmtun.sh <<"EOF"
my_ip=`/sbin/ifconfig ${my_netdev} | grep 'inet ' | awk '{print $2}'`
EOF

# we turn back on variable expansion, so we use parameter variables
# set at top of script
cat >> /usr/local/bin/vmtun.sh <<EOF
tunnel_ip='${tunnel_ip}'

# port numbers to tunnel services to, use '0' to disable tunnel for
# that services
ssh_port='${ssh_port}'
http_port='${http_port}'
https_port='${https_port}'
vnc_port='${vnc_port}'
rdp_port='${rdp_port}'
jupyterhub_port='${jupyterhub_port}'
rstudio_port='${rstudio_port}'

EOF

cat >> /usr/local/bin/vmtun.sh <<"EOF"
function d_start ( ) 
{
    echo  "vmtun: starting tunnels from: $my_ip to: ${tunnel_ip}"
    
    autossh_cmd='/usr/bin/autossh -i /root/.ssh/vmtun.pem -oStrictHostKeyChecking=false -oServerAliveInterval=30 -oServerAliveCountMax=3 -M 0 -N -v -R '
    tunnel_credentials="root@${tunnel_ip}"

    # ssh tunnel
    if [ ${ssh_port} -gt 1000 ]
    then
       ${autossh_cmd} ${tunnel_ip}:${ssh_port}:${my_ip}:22 ${tunnel_credentials} &
    fi
    
    # http tunnel
    if [ ${http_port} -gt 1000 ]
    then
       ${autossh_cmd} ${tunnel_ip}:${http_port}:${my_ip}:8080 ${tunnel_credentials} &
    fi
    
    # https tunnel
    if [ ${https_port} -gt 1000 ]
    then
       ${autossh_cmd} ${tunnel_ip}:${https_port}:${my_ip}:443 ${tunnel_credentials} &
    fi
    
    # vnc tunnel
    if [ ${vnc_port} -gt 1000 ]
    then
       ${autossh_cmd} ${tunnel_ip}:${vnc_port}:${my_ip}:5901 ${tunnel_credentials} &
    fi
    
    # rdp tunnel
    if [ ${rdp_port} -gt 1000 ]
    then
       ${autossh_cmd} ${tunnel_ip}:${rdp_port}:${my_ip}:3389 ${tunnel_credentials} &
    fi
    
    # jupyterhub tunnel
    if [ ${jupyterhub_port} -gt 1000 ]
    then
       ${autossh_cmd} ${tunnel_ip}:${jupyterhub_port}:${my_ip}:8000 ${tunnel_credentials} &
    fi
    
    # rstudio server tunnel
    if [ ${rstudio_port} -gt 1000 ]
    then
       ${autossh_cmd} ${tunnel_ip}:${rstudio_port}:${my_ip}:8787 ${tunnel_credentials} &
    fi
    
    echo $!>/var/run/vmtun.pid
    echo  "PID is $(cat /var/run/vmtun.pid)" 
}
 
function d_stop ( ) 
{ 
    echo  "vmtun: stopping Service (PID = $ (cat /var/run/vmtun.pid) )" 
    kill `cat /var/run/vmtun.pid` 
    rm  /var/run/vmtun.pid
 }
 
function d_status ( ) 
{ 
    if [ -e /var/run/vmtun.pid ]; then
	echo vmtun.sh is running, pid=`cat /var/run/vmtun.pid`
	echo "tunnel ip: ${tunnel_ip}"
	echo "        ssh port: ${ssh_port}"
	echo "       http port: ${http_port}"
	echo "      https port: ${https_port}"
	echo "        vnc port: ${vnc_port}"
	echo "        rdp port: ${rdp_port}"
	echo " jupyterhub port: ${jupyterhub_port}"
	echo "    rstudio port: ${rstudio_port}"
    else
	echo vmtun.sh is NOT running
	exit 1
    fi
}
 
# Management instructions of the service 
case  "$1"  in 
    start)
	d_start
	;; 
    stop)
	d_stop
	;; 
    reload)
	d_stop
	sleep  1
	d_start
	;; 
    status)
	d_status
	;; 
    *) 
	echo  "Usage: $ 0 {start | stop | reload | status}" 
	exit  1 
	;; 
esac
 
exit  0
EOF

# needs to be an executable shell script
chmod u+x /usr/local/bin/vmtun.sh


# create the systemd config file to add the service
cat > /etc/systemd/system/vmtun.service <<EOF
[Unit]
Description = vmtun daemon
After = network.target

[Service]
Type = forking
ExecStart = /usr/local/bin/vmtun.sh start
ExecStop = /usr/local/bin/vmtun.sh stop
ExecReload = /usr/local/bin/vmtun.sh reload

[Install]
WantedBy = multi-user.target
EOF

# enable the service to start on boot and start it
echo "Starting vmtun tunnel services..."
systemctl enable vmtun
systemctl start vmtun
/usr/local/bin/vmtun.sh status


# allow password entry for sshd server and restart service to get new configuration
# if it is set to no, make it yes
#echo "Allowing password authentication via ssh (for sftp)..."
#sed -i 's/^PasswordAuthentication no$/PasswordAuthentication yes/g' /etc/ssh/sshd_config
#systemctl restart ssh

# change password for default user
#echo -e "ubuntu\nubuntu" | passwd ubuntu > /dev/null 2>&1
