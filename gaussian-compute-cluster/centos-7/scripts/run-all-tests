#!/usr/bin/env bash
#
# Out gaussian install comes with a number of example com/job files we can use as tests.  The
# log file outputs run from a working install of gaussian are included under the $TEST_DIR/ia64
# directory.  This script basically reruns the specified tests, then compares that the output
# we compute is close enough to the reference/correct output.  If output matches we declare the
# test as passing.
#
# This script accepts a single required parameter.  You can invoke the tests to run
# in serial, only on a single node/cpu:
#
#    $ run-all-tests serial
#
# If you want to test the parallel gaussian/linda setup and performance, invoke with parallel
# option
#    $ run-all-tests parallel

# we expect 1 command line argument, see if it is provided
usage() {
  echo "Usage: run-all-tests [serial|parallel]"
  echo "       serial   - run in serial mode using only a single cpu, the current host"
  echo "       parallel - run using gaussian/linda parallel environment"
  exit 0
}

if [ $# -eq 0 ]
then
  usage
fi

execution_mode=$1
if [ "${execution_mode}" != "serial" ] &&
   [ "${execution_mode}" != "parallel" ]
then
  usage
fi

TEST_DIR="$g09root/g09/tests"
COM_DIR="$TEST_DIR/com"
RES_DIR="$TEST_DIR/ia64"

# set up directory for test scratch files
mkdir -p gauss_tests
cd gauss_tests

# run the tests
echo "********** Running Gaussian09 tests (execution mode: ${execution_mode})"
# the following actually only runs tests 000-099, there are actaully more than that
test_files=`ls $COM_DIR/test00?.com`
# the following will run all the tests which range from 000-999 (with a few numbers skipped/missing)
#test_files=`ls $COM_DIR/test???.com`

start=$(date +%s)
for comfile in $test_files
do
    basefile=`basename $comfile .com`
    resfile="$RES_DIR/$basefile.log"
    logfile="$basefile.log"

    # run the g09 command
    #echo "Running g09 < $comfile > $logfile"
    #g09 < $comfile > $logfile  # can just run directly by hand if you have a good comfile created
    if [ "${execution_mode}" == "serial" ]
    then    
      g09serial $comfile
    elif [ "${execution_mode}" == "parallel" ]
    then
      g09parallel $comfile
    else
      usage
    fi
    
    # see if test passed or failed
    grep "Symmetry" $resfile | round-floats > $basefile.res
    grep "Symmetry" $logfile | round-floats > $basefile.out

    o1=`diff $basefile.res $basefile.out`
    t1=$?
    o2=`grep "Normal termination" $logfile`
    t2=$?

    if [ $t1 -eq 0 ] && [ $t2 -eq 0 ]; then
        echo -e "$basefile: \e[32mPASSED\e[39m"
    else
        if [ $basefile == "test022" ] || [ $basefile == "test099" ]; then # test 22 and 99 sometimes have correct but slightly different results
            echo -e "$basefile: Skipped..."
        else
            echo -e "$basefile: \e[31mFAILED\e[39m"
            #echo "$o1"
        fi
    fi
    
done
end=$(date +%s)

echo "Elapsed time for tests to complete: $(($end - $start)) sec"

# clean up
echo ""
cd ..
rm -rf gauss_tests
