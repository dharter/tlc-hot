#!/usr/bin/env python2
# calculate digits of pi, as many as we are asked to calculate
import sys

def make_pi(num_digits):
    q, r, t, k, m, x = 1, 0, 1, 1, 3, 3
    for j in range(num_digits):
        if 4 * q + r - t < m * t:
            yield m
            q, r, t, k, m, x = 10*q, 10*(r-m*t), t, k, (10*(3*q+r))//t - 10*m, x
        else:
            q, r, t, k, m, x = q*k, (2*q+r)*x, t*x, k+1, (q*(7*k+2)+r*x)//(t*x), x+2


if __name__ == "__main__":

    if len(sys.argv) != 2:
        print "usage: calcpi.py num_digits"
        sys.exit(1)
        
    num_digits = int(float(sys.argv[1]))
    digits = [str(digit) for digit in make_pi(num_digits * 5)]

    digits = digits[:1] + ['.'] + digits[1:]
    big_string = "".join(digits)
    print "Here are the first %d digits of pi:\n %s" % (num_digits, big_string)
