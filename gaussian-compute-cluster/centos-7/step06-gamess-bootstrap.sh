#!/bin/bash
#

# check we have required prerequisites for build install
yum install -y csh gcc-gfortran patch openblas openblas-devel openblas-static

# extract gamess source
cd /opt
tar xvfz /home/centos/gamess-current.tar.gz
chown -R root:root gamess
chmod -R g-s gamess

# configure
cd gamess
./config


# Do not forget to run './tools/download-libxc.csh' for downloading libxc,
# And do not forget to run 'make libxc -j$(nproc)' before 'make modules' for compiling of LibXC...

# compile ddi
cd ddi
./compddi >& compddi.log
mv ddikick.x ..
cd ..

# compile gamess
./compall >& compall.log
