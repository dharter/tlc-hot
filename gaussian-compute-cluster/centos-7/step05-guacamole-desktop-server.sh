#!/bin/bash
#

# set up guacamole server
yum install -y cairo-devel libjpeg-devel libpng-devel uuid-devel freerdp-devel pango-devel libssh2-devel libssh-dev tomcat libvncserver-devel libtelnet-devel tomcat-admin-webapps tomcat-webapps gcc terminus-fonts

# firewalld gets added for dependencies, need to make sure it doesn't start or it interferes
# with torque
systemctl disable firewalld
systemctl stop firewalld

# get guacamole server source
cd /root
wget https://downloads.apache.org/guacamole/1.2.0/source/guacamole-server-1.2.0.tar.gz

# configure and build
tar xvfz guacamole-server-1.2.0.tar.gz
cd guacamole-server-1.2.0
./configure --with-init-dir=/etc/init.d --enable-guacenc
make
make install
ldconfig

# start and enable the guacamole server
systemctl daemon-reload
systemctl start guacd
systemctl enable guacd

# install Guacamole client
mkdir /etc/guacamole
wget https://downloads.apache.org/guacamole/1.1.0/binary/guacamole-1.1.0.war -O /etc/guacamole/guacamole.war
ln -s /etc/guacamole/guacamole.war /var/lib/tomcat/webapps/
systemctl enable tomcat 
systemctl restart tomcat
systemctl restart guacd

mkdir /etc/guacamole/{extensions,lib}
echo "GUACAMOLE_HOME=/etc/guacamole" >> /etc/default/tomcat

cat <<EOF > /etc/guacamole/guacamole.properties
guacd-hostname: localhost
guacd-port:     4822
user-mapping:   /etc/guacamole/user-mapping.xml
auth-provider:  net.sourceforge.guacamole.net.basic.BasicFileAuthenticationProvider
EOF

ln -s /etc/guacamole /usr/share/tomcat/.guacamole

# create desktop connections file
cat <<EOF > /etc/guacamole/user-mapping.xml
<user-mapping>
        
    <!-- Per-user authentication and config information -->

    <!-- A user using md5 to hash the password
         guacadmin user and its md5 hashed password below is used to 
             login to Guacamole Web UI-->
    <authorize 
            username="gaussian"
            password="gaussian">


        <!-- Allow direct connection to ssh command line terminal -->
        <connection name="Gaussian SSH Terminal Access">
            <protocol>ssh</protocol>
            <param name="hostname">127.0.0.1</param>
            <param name="port">22</param>
            <param name="server-alive-interval">600</param>
            <param name="username">gaussian</param>
            <param name="private-key">-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEA1gKT7deB1/cqquWKIpR8VwIiVpcf+i5eC2akh4+gMM3r8MSS
g62TBU23qMkVyBhuvW4PDHgvF2YaMlyZmye0ZNTvKpxnrX4CdhTA0Dastv2ZSZP8
oj71MgRRsv1ZBnsNhE5lZIKqcxM5RSrhxJmRmK804FTSdDsz/hi0qIlQtvlLXW/y
VoRVBBuj13oK5E1GG+M1VKzmXjruvGwwNh2y91+m6aCcn+vz6r1Giz6LVFxOeFZ8
/nomsQCBdhxDyEZqpl56rSWW+pbJ1MT4m6YBUYvE0DYCwBjq8TFWVHeYHrWBfQWp
23XEXZNJd6u0vTMUwVID9K+i3ldDzED/IGZB/QIDAQABAoIBAE5CtxH9AzCU5HeG
ZVNr9WRkFWaQ8VKt7hpBON2R+kSeqS9s0x9wNBl2aDlC/8IinSyMuNgYgzANWHse
PdXtWRyeea+dbpSVRhUkVbZOqC5q4slzYql6WHQn/DB2UT37dd/yQ0XUOrzGROU0
X0dj2Uh6md2HZ0a197duFXKkdG3UZPeVvzUjmOT59C3dvDbTC5ZE7NTLAhMK/fx6
qmAUq15cdRf6C8WtEvmkEl0vukS5WlDWUr4pKiaAPjJl6w09Ii+Ucswkwb9/x9WO
6KJvlnh+B+9TrCWZ6IOBuWLIeNQylXBwF4n1E5/9eK6qjx2G/Lpj5WRyQf0zl4Y9
xf92dskCgYEA9xp+y5zSo6rPAyObbK20pXpKtdowrZhrcvwzT41nr6/xDHklNg0r
30lN/1FP5dZC2VR9MaGXuAKF5qGAOtiraeZNRLqTCXgD07A+RduWZYil9E79RyJ9
ltFALol9vYHAoFB5X0Occ1zQj82oiyrYT8kNni01n8QASoM5RKiKVncCgYEA3bcR
H8XR7hnMMuVI1kPvW/nX3zutj/p12iQRlK6eF2n1HUPyuBQOHWdFk5D85txAu+/d
RcKpY2I/2PLx0M0qVmOpszelT6jiUCIaqRRb/r12e/Rar2y/Y56gKBCv2sbg0rMv
uSC9gWbIDxh92xou6D9sM0Wh5K/Yv/fxBC5HJCsCgYB0gbWO4x4dtQdt/3fkwfIO
o55eJ8YT0222BVtcAtwEZfxNWUFGPueUm8d+viPhxig2uDNfF0WlqOrhVvxSwp3v
jHqaES4KpN+JC+wQm/xJWyiEDEo3I/f7CCYuyvzSQT9FVywi0ooNipF7fAgftc/P
rogRMFe8QHmyqDR0PtMa+QKBgCNJBUKnOvfHx0W6U9M10Jw0sPRoDQFC/QqQfvPI
roRPaqETtUcyeeSYEOyhQH6FQxAl5nM5vbeBZWPjljSC0mAZUGKjUn3RBON36gJK
qTFrebXUd7I/fl4eWqLFtRb+W71sowW1fjNf5irw3ObHzftMTK4EQPWiPr6eeQ8P
RmctAoGBAKtfyfGHLW0vz3mTd/6qVxFIG7HJk9nagAMKNL/NYZ7XnQ/6Y9EyDOCv
ovWNTFWDY2rqNcdpRSENQyC8CoC7ZbUFxuXWteqIC+s4fs7Ju4+CXhdFTOOAgJUW
C3VtfpiMPgJf/g5h9sKJbqY4RiUnfTxbSaKIx8vqLbbeOhc6Mtyz
-----END RSA PRIVATE KEY-----</param>
            <param name="enable-sftp">true</param>
            <param name="sftp-root-directory">\/home\/gaussian</param>
        </connection>

        <!-- Allow direct connection to xrdp server at port 3389 remote connection -->
        <connection name="Gaussian Remote Desktop Protocol (RDP) Desktop Session">
            <protocol>rdp</protocol>
            <param name="hostname">127.0.0.1</param>
            <param name="port">3389</param>
            <param name="username">gaussian</param>
            <param name="password">gaussian</param>
            <param name="ignore-cert">true</param>
            <param name="enable-sftp">true</param>
            <param name="sftp-username">gaussian</param>
            <param name="sftp-private-key">-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEA1gKT7deB1/cqquWKIpR8VwIiVpcf+i5eC2akh4+gMM3r8MSS
g62TBU23qMkVyBhuvW4PDHgvF2YaMlyZmye0ZNTvKpxnrX4CdhTA0Dastv2ZSZP8
oj71MgRRsv1ZBnsNhE5lZIKqcxM5RSrhxJmRmK804FTSdDsz/hi0qIlQtvlLXW/y
VoRVBBuj13oK5E1GG+M1VKzmXjruvGwwNh2y91+m6aCcn+vz6r1Giz6LVFxOeFZ8
/nomsQCBdhxDyEZqpl56rSWW+pbJ1MT4m6YBUYvE0DYCwBjq8TFWVHeYHrWBfQWp
23XEXZNJd6u0vTMUwVID9K+i3ldDzED/IGZB/QIDAQABAoIBAE5CtxH9AzCU5HeG
ZVNr9WRkFWaQ8VKt7hpBON2R+kSeqS9s0x9wNBl2aDlC/8IinSyMuNgYgzANWHse
PdXtWRyeea+dbpSVRhUkVbZOqC5q4slzYql6WHQn/DB2UT37dd/yQ0XUOrzGROU0
X0dj2Uh6md2HZ0a197duFXKkdG3UZPeVvzUjmOT59C3dvDbTC5ZE7NTLAhMK/fx6
qmAUq15cdRf6C8WtEvmkEl0vukS5WlDWUr4pKiaAPjJl6w09Ii+Ucswkwb9/x9WO
6KJvlnh+B+9TrCWZ6IOBuWLIeNQylXBwF4n1E5/9eK6qjx2G/Lpj5WRyQf0zl4Y9
xf92dskCgYEA9xp+y5zSo6rPAyObbK20pXpKtdowrZhrcvwzT41nr6/xDHklNg0r
30lN/1FP5dZC2VR9MaGXuAKF5qGAOtiraeZNRLqTCXgD07A+RduWZYil9E79RyJ9
ltFALol9vYHAoFB5X0Occ1zQj82oiyrYT8kNni01n8QASoM5RKiKVncCgYEA3bcR
H8XR7hnMMuVI1kPvW/nX3zutj/p12iQRlK6eF2n1HUPyuBQOHWdFk5D85txAu+/d
RcKpY2I/2PLx0M0qVmOpszelT6jiUCIaqRRb/r12e/Rar2y/Y56gKBCv2sbg0rMv
uSC9gWbIDxh92xou6D9sM0Wh5K/Yv/fxBC5HJCsCgYB0gbWO4x4dtQdt/3fkwfIO
o55eJ8YT0222BVtcAtwEZfxNWUFGPueUm8d+viPhxig2uDNfF0WlqOrhVvxSwp3v
jHqaES4KpN+JC+wQm/xJWyiEDEo3I/f7CCYuyvzSQT9FVywi0ooNipF7fAgftc/P
rogRMFe8QHmyqDR0PtMa+QKBgCNJBUKnOvfHx0W6U9M10Jw0sPRoDQFC/QqQfvPI
roRPaqETtUcyeeSYEOyhQH6FQxAl5nM5vbeBZWPjljSC0mAZUGKjUn3RBON36gJK
qTFrebXUd7I/fl4eWqLFtRb+W71sowW1fjNf5irw3ObHzftMTK4EQPWiPr6eeQ8P
RmctAoGBAKtfyfGHLW0vz3mTd/6qVxFIG7HJk9nagAMKNL/NYZ7XnQ/6Y9EyDOCv
ovWNTFWDY2rqNcdpRSENQyC8CoC7ZbUFxuXWteqIC+s4fs7Ju4+CXhdFTOOAgJUW
C3VtfpiMPgJf/g5h9sKJbqY4RiUnfTxbSaKIx8vqLbbeOhc6Mtyz
-----END RSA PRIVATE KEY-----</param>
            <param name="sftp-root-directory">\/home\/gaussian</param>
        </connection>

    </authorize>

    <!-- A user using md5 to hash the password
         guacadmin user and its md5 hashed password below is used to 
             login to Guacamole Web UI-->
    <authorize 
            username="centos"
            password="centos">


        <!-- Allow direct connection to ssh command line terminal -->
        <connection name="Gaussian SSH Terminal Access">
            <protocol>ssh</protocol>
            <param name="hostname">127.0.0.1</param>
            <param name="port">22</param>
            <param name="server-alive-interval">600</param>
            <param name="username">centos</param>
            <param name="private-key">-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAs5kvgjwLiww/hRCrgHCkfg8QFNnf3oWffxgQXdVCjQghMz6w
g9+j3ZDsC6ONQOC2PLykYhniW7db79BPkQ34aTbTNhUyEAGN3dB3oeYAB7yVaCwW
8LdHKtIXnhTWkEcgKiQGXt7uCTs9AwZbfSGHMJDQmnjRI3NaMtrasEZfv0GUPiYD
qbsfmbFtm9lTHwrSp2zUSBdDZKELICKQu53g8Ezi5aFh7BlEA4kR/tVVIkXhBpR9
zSCRxPCNXaYItoqBUlFZVnfHLt9q9mgpMKhR9omcGZrWKlqoMr2lrslaJ9nodUJk
PMUYhnd2kuq/xR6JpCrdyd0SLV4LHM3olWcbSQIDAQABAoIBACitChiJ9udQJ9bA
VsdpO6gdSzwduFlSlDQZxCMsWL1OWQ6dJSdI8HkPCJb+PNCz1DJfWy+owx6Yoabn
3A1zR460zxNRq5v6biINw/qT9+/Io2ILkkqEiAbHvHeeKpZUtZE1xYeZH/pRQ1k7
CF+Hx3GVatmBgYRw7L3V2W/REIBlPcPFHcXlt6PDHlZL6tyEDblp2QVaNhTTA6X4
cDmnwM9Sw8/l5i7ao0FpGt0I5r9R+QvZJW0l15Nqa/l/48rmUToP1Hm9QVSSJBp5
fXqrOOZoNpW4idAdMFPKeWLRFtALRYV2y2G+owu5zm8EKV7d/skSSrbz6URgVw0K
CeduVW0CgYEA2omgR9l3ypFUUnh045EcZcK1fGt6cMJgxhLrHFceTgGdu+FNO/bG
ouEty2Eg91jmljyvc9ivFRtCB1JLQNEQvdbl+11wftWBDlivWKdTKMbv+03Py+uO
gE0dkML8VFvmgFV3JAOd3gTTWr6Du/Bu6Pbvkar/jWj+CleF3+AtCjMCgYEA0mK9
AGNbFurSzJH1q+3p1V5rcrZR4FPTkd7w4DP/5voBdCKIlA0k7CicblV6Nmy5pNJy
Kvpn/RBNcyAjyrGuMV/ovq9npDXq4Vb/i829erPZfpz/13m/kzWWMi8/Fa3V9L69
E4syoe55LX5uoa0zb8eeVZowbAwCZSjzioCMwJMCgYEAhS3oGsFBypYkgBneiiyN
1MoDFe7d8EfU+ADhfSTuV/WOLchQ7CEozmxSHyZzE6n+5EHZXs0HbeJtq86QA9Ek
1bxX7Yb1HqZ4fsi3G1zlRe1upIS4BPrpnfDg+q6JXdOftkffUnhNxdbFAwElgOw2
ewy51NkT4hbA8oaCFHwmlpcCgYEAijOYamfaD3zkSsPa+T0JLUOgG7POpykJwzoL
S4x9HxK937dLts3HomavE/+Mj7HuZWpk2vyxUNjjt6u2N1Fu1zpkntaEo6dTiwFm
T0UwoikAx56EpdxxT1eVOZQ1wZz2/pZJiEwZT6xi59K1A/nN3zDnPt5Rzrg43pD/
Is19KIcCgYAi91j5kK1EmQaQqSRKCxU3+Qv6klzlb8IETzOPyUxT8Qyi2gZc81Wr
eacTPdoeW1RbeG2UNFkPm48mDNSJvDfGYsId3flIrrY/UunQ0kTtIuuZuUqWxen/
oU3FnFw8sU9GsBhemWFH8tAV8+tfa08ryA9aeB1f5bRGupVDp+0bhw==
-----END RSA PRIVATE KEY-----</param>
            <param name="enable-sftp">true</param>
            <param name="sftp-root-directory">\/home\/centos</param>
        </connection>

        <!-- Allow direct connection to xrdp server at port 3389 remote connection -->
        <connection name="Gaussian Remote Desktop Protocol (RDP) Desktop Session">
            <protocol>rdp</protocol>
            <param name="hostname">127.0.0.1</param>
            <param name="port">3389</param>
            <param name="username">centos</param>
            <param name="password">centos</param>
            <param name="ignore-cert">true</param>
            <param name="enable-sftp">true</param>
            <param name="sftp-username">centos</param>
            <param name="sftp-private-key">-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAs5kvgjwLiww/hRCrgHCkfg8QFNnf3oWffxgQXdVCjQghMz6w
g9+j3ZDsC6ONQOC2PLykYhniW7db79BPkQ34aTbTNhUyEAGN3dB3oeYAB7yVaCwW
8LdHKtIXnhTWkEcgKiQGXt7uCTs9AwZbfSGHMJDQmnjRI3NaMtrasEZfv0GUPiYD
qbsfmbFtm9lTHwrSp2zUSBdDZKELICKQu53g8Ezi5aFh7BlEA4kR/tVVIkXhBpR9
zSCRxPCNXaYItoqBUlFZVnfHLt9q9mgpMKhR9omcGZrWKlqoMr2lrslaJ9nodUJk
PMUYhnd2kuq/xR6JpCrdyd0SLV4LHM3olWcbSQIDAQABAoIBACitChiJ9udQJ9bA
VsdpO6gdSzwduFlSlDQZxCMsWL1OWQ6dJSdI8HkPCJb+PNCz1DJfWy+owx6Yoabn
3A1zR460zxNRq5v6biINw/qT9+/Io2ILkkqEiAbHvHeeKpZUtZE1xYeZH/pRQ1k7
CF+Hx3GVatmBgYRw7L3V2W/REIBlPcPFHcXlt6PDHlZL6tyEDblp2QVaNhTTA6X4
cDmnwM9Sw8/l5i7ao0FpGt0I5r9R+QvZJW0l15Nqa/l/48rmUToP1Hm9QVSSJBp5
fXqrOOZoNpW4idAdMFPKeWLRFtALRYV2y2G+owu5zm8EKV7d/skSSrbz6URgVw0K
CeduVW0CgYEA2omgR9l3ypFUUnh045EcZcK1fGt6cMJgxhLrHFceTgGdu+FNO/bG
ouEty2Eg91jmljyvc9ivFRtCB1JLQNEQvdbl+11wftWBDlivWKdTKMbv+03Py+uO
gE0dkML8VFvmgFV3JAOd3gTTWr6Du/Bu6Pbvkar/jWj+CleF3+AtCjMCgYEA0mK9
AGNbFurSzJH1q+3p1V5rcrZR4FPTkd7w4DP/5voBdCKIlA0k7CicblV6Nmy5pNJy
Kvpn/RBNcyAjyrGuMV/ovq9npDXq4Vb/i829erPZfpz/13m/kzWWMi8/Fa3V9L69
E4syoe55LX5uoa0zb8eeVZowbAwCZSjzioCMwJMCgYEAhS3oGsFBypYkgBneiiyN
1MoDFe7d8EfU+ADhfSTuV/WOLchQ7CEozmxSHyZzE6n+5EHZXs0HbeJtq86QA9Ek
1bxX7Yb1HqZ4fsi3G1zlRe1upIS4BPrpnfDg+q6JXdOftkffUnhNxdbFAwElgOw2
ewy51NkT4hbA8oaCFHwmlpcCgYEAijOYamfaD3zkSsPa+T0JLUOgG7POpykJwzoL
S4x9HxK937dLts3HomavE/+Mj7HuZWpk2vyxUNjjt6u2N1Fu1zpkntaEo6dTiwFm
T0UwoikAx56EpdxxT1eVOZQ1wZz2/pZJiEwZT6xi59K1A/nN3zDnPt5Rzrg43pD/
Is19KIcCgYAi91j5kK1EmQaQqSRKCxU3+Qv6klzlb8IETzOPyUxT8Qyi2gZc81Wr
eacTPdoeW1RbeG2UNFkPm48mDNSJvDfGYsId3flIrrY/UunQ0kTtIuuZuUqWxen/
oU3FnFw8sU9GsBhemWFH8tAV8+tfa08ryA9aeB1f5bRGupVDp+0bhw==
-----END RSA PRIVATE KEY-----</param>
            <param name="sftp-root-directory">\/home\/centos</param>
        </connection>

    </authorize>

    <authorize 
            username="langel"
            password="aivu4Voh">


        <!-- Allow direct connection to ssh command line terminal -->
        <connection name="Gaussian SSH Terminal Access">
            <protocol>ssh</protocol>
            <param name="hostname">127.0.0.1</param>
            <param name="port">22</param>
            <param name="server-alive-interval">600</param>
            <param name="username">langel</param>
            <param name="private-key">-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEA1lItmDq7KJcBdjbMe+Y73YEH7QfpRALiYhh1RiZTuNXvxgup
onFh8xTjuhYOLFim80zhDA5Vv6P9unoekvE2qBGtwyYdBhdGajnoPdqCDs15BkCg
0G7DaIzodnLaDxsKd0/dQnAekkO2YluVk7+qUKPgKScMLo+sXUyBgTUfp4/U/8Oy
sjPlTw204PzPmv1QePkCZpDjLzQpS/ank2dIW2XPIkxn2XCdX+VGTkXXBdcWvjms
LV3TQnIl/Zqyt12/DtmxJBhgeCiExftYAxl5PhdWPrwUO6qgytkFpsksZV3PIu/q
Gc44sv8aOjCiB9e/65XpcUKfJ7JovmNmua0U0wIDAQABAoIBAF+QCPCFz4oLuwUg
7hCY2jysGgKgsmw29QZUbYY39MDH87/U13CtKcWfnOMOjodLiDWYJAiiI63ty2bo
qNVQZNSP/XC0f+CFZEpdQ6bA1ZkQGVSmYI5PtSXbHpQiJgTyQ7pri/bh01/YfWxh
9dIJdkG6sGrpFE92lJ6NTHl2XITzllcR+3hY96REadeGj8BaY2/RjVYGJTEnl0ZB
6igiuKdqeieGi7vV0v3vnv1iuxwBu2kBN6fbrO0lnAFK8931oxSiKnoF9Cxzv1RI
wk0Ymy3NowoU5DYeE7TOiRJMYkxOVVLZEufeBtQX4UYT9ID1Az8KtWgSDk5xagT7
1bWM1ZECgYEA7auQXexUFXt0ZrDrkt0AfI59bjFb1qaiixZaSpuMIXCUuD0hZ82F
MN4jUoagQ0xprtPueevhXEBdKmcf6o0L+orkQALUCIR+BoMVy6RjLybV65qNTxs1
t0VrEyQ80J264pjdwMNkiNNNPbMjJSzN77e7f4vE5b950VUDN0ZrQSUCgYEA5tme
1eZxKqCYN9lVqc15mNVKhwSpM+1KWyzbfOQeotwkSHrPIFWicQMKQOYQ1ihc/mof
LXsMdqaHoWGuKTTWHM04iaze8pJNXup3mT/MB9cWGN+Ccq5x2Pk90WxEKqUP1opC
m/yt7OYH+6C10ygDNMFe/dHfS6+asA9siBg2iJcCgYAhufremS7FbaNy/yAu0xXC
lyXNgUubBkKCokVl2PTlueggzOv0owGHkb5Jc7lCU5GbsY9h1ctBIBEYd1HcIu/U
Oelx+/TC0KphmjB5mXZJtqUcre3nj+DrhPpQ2Q+YOwnOJgsx04hssi9/1e/i0pzW
q6f7zYW/ZJCQ2BOAwIlv0QKBgFv7QCeYf2TtwlkWduSGo7m62sw7ljPDi8oScI4t
0usvbiGXFWqb1HLVNEOxp+m9AXi5zhxwGAI6znYGFsACdoHZX3qz782Vb9Cu+eKN
If3JvNtCS5boXSKIBRKp4etgA5SHWI0QvDhVW77uRSSSn9xoZDIcaq2clbUrICKG
C0yXAoGBAI0VvbLZag+EcRQV59VVlOf5xvQw6u0cZHJ8ucO5aGsn6A3MCt8Swox7
5DnYKkEbHhehtRZC1uPD9GEz1aheffQf3Z4Gi0kT7346mTpR3geVU/mAumoYIKhH
A9e0PB0c9cF8PoZySjSvx7bdftZsnFNjk6ziCvpBJusYEkwPHWko
-----END RSA PRIVATE KEY-----</param>
            <param name="enable-sftp">true</param>
            <param name="sftp-root-directory">\/home\/langel</param>
        </connection>

        <!-- Allow direct connection to xrdp server at port 3389 remote connection -->
        <connection name="Gaussian Remote Desktop Protocol (RDP) Desktop Session">
            <protocol>rdp</protocol>
            <param name="hostname">127.0.0.1</param>
            <param name="port">3389</param>
            <param name="username">langel</param>
            <param name="password">aivu4Voh</param>
            <param name="ignore-cert">true</param>
            <param name="enable-sftp">true</param>
            <param name="sftp-username">langel</param>
            <param name="sftp-private-key">-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEA1lItmDq7KJcBdjbMe+Y73YEH7QfpRALiYhh1RiZTuNXvxgup
onFh8xTjuhYOLFim80zhDA5Vv6P9unoekvE2qBGtwyYdBhdGajnoPdqCDs15BkCg
0G7DaIzodnLaDxsKd0/dQnAekkO2YluVk7+qUKPgKScMLo+sXUyBgTUfp4/U/8Oy
sjPlTw204PzPmv1QePkCZpDjLzQpS/ank2dIW2XPIkxn2XCdX+VGTkXXBdcWvjms
LV3TQnIl/Zqyt12/DtmxJBhgeCiExftYAxl5PhdWPrwUO6qgytkFpsksZV3PIu/q
Gc44sv8aOjCiB9e/65XpcUKfJ7JovmNmua0U0wIDAQABAoIBAF+QCPCFz4oLuwUg
7hCY2jysGgKgsmw29QZUbYY39MDH87/U13CtKcWfnOMOjodLiDWYJAiiI63ty2bo
qNVQZNSP/XC0f+CFZEpdQ6bA1ZkQGVSmYI5PtSXbHpQiJgTyQ7pri/bh01/YfWxh
9dIJdkG6sGrpFE92lJ6NTHl2XITzllcR+3hY96REadeGj8BaY2/RjVYGJTEnl0ZB
6igiuKdqeieGi7vV0v3vnv1iuxwBu2kBN6fbrO0lnAFK8931oxSiKnoF9Cxzv1RI
wk0Ymy3NowoU5DYeE7TOiRJMYkxOVVLZEufeBtQX4UYT9ID1Az8KtWgSDk5xagT7
1bWM1ZECgYEA7auQXexUFXt0ZrDrkt0AfI59bjFb1qaiixZaSpuMIXCUuD0hZ82F
MN4jUoagQ0xprtPueevhXEBdKmcf6o0L+orkQALUCIR+BoMVy6RjLybV65qNTxs1
t0VrEyQ80J264pjdwMNkiNNNPbMjJSzN77e7f4vE5b950VUDN0ZrQSUCgYEA5tme
1eZxKqCYN9lVqc15mNVKhwSpM+1KWyzbfOQeotwkSHrPIFWicQMKQOYQ1ihc/mof
LXsMdqaHoWGuKTTWHM04iaze8pJNXup3mT/MB9cWGN+Ccq5x2Pk90WxEKqUP1opC
m/yt7OYH+6C10ygDNMFe/dHfS6+asA9siBg2iJcCgYAhufremS7FbaNy/yAu0xXC
lyXNgUubBkKCokVl2PTlueggzOv0owGHkb5Jc7lCU5GbsY9h1ctBIBEYd1HcIu/U
Oelx+/TC0KphmjB5mXZJtqUcre3nj+DrhPpQ2Q+YOwnOJgsx04hssi9/1e/i0pzW
q6f7zYW/ZJCQ2BOAwIlv0QKBgFv7QCeYf2TtwlkWduSGo7m62sw7ljPDi8oScI4t
0usvbiGXFWqb1HLVNEOxp+m9AXi5zhxwGAI6znYGFsACdoHZX3qz782Vb9Cu+eKN
If3JvNtCS5boXSKIBRKp4etgA5SHWI0QvDhVW77uRSSSn9xoZDIcaq2clbUrICKG
C0yXAoGBAI0VvbLZag+EcRQV59VVlOf5xvQw6u0cZHJ8ucO5aGsn6A3MCt8Swox7
5DnYKkEbHhehtRZC1uPD9GEz1aheffQf3Z4Gi0kT7346mTpR3geVU/mAumoYIKhH
A9e0PB0c9cF8PoZySjSvx7bdftZsnFNjk6ziCvpBJusYEkwPHWko
-----END RSA PRIVATE KEY-----</param>
            <param name="sftp-root-directory">\/home\/langel</param>
        </connection>

    </authorize>

</user-mapping>
EOF

chmod 600 /etc/guacamole/user-mapping.xml
chown tomcat:tomcat /etc/guacamole/user-mapping.xml

cat <<EOF >> /etc/tomcat/catalina.properties

# added by guacamole install/setup
guacamole.home=/etc/guacamole
EOF

systemctl daemon-reload
systemctl restart guacd
systemctl restart tomcat 
