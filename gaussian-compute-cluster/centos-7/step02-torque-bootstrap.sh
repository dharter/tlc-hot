#/usr/bin/env bash
#
# Using these instructions: https://www.webmo.net/support/torque.html
# Official torque repository: https://github.com/adaptivecomputing/torque
# Install/admin docs: http://docs.adaptivecomputing.com/torque/6-1-1/adminGuide/help.htm

node_count=9
num_cpu=16
base_hostname="gauss"

# install required dependencies for torque
yum install -y openssl openssl-devel libxml2-devel boost-devel hwloc hwloc-devel  tcl tk tcl-devel tk-devel 

# build and install from repository
# clone the repository and set it up
git clone https://github.com/adaptivecomputing/torque.git -b 6.1.2 torque-6.1.2
cd torque-6.1.2
./autogen.sh

# install torque on headnode with ssh
#./configure --enable-cgroups --enable-nvidia-gpus
./configure --enable-cgroups --enable-gui
make
make install
source /etc/profile.d/torque.sh # add /usr/local/bin and /usr/local/sbin to path for following steps

# initialize serverdb
./torque.setup root
qterm

# node database
config=/var/spool/torque/server_priv/nodes
cat > ${config} <<EOF
# torque/pbs compute nodes
EOF
for ((node_num=2; node_num<=${node_count}; node_num++))
do
    printf "%s-%02d np=%d\n" ${base_hostname} ${node_num} ${num_cpu} >> ${config}
done

# start pbs_server
systemctl enable pbs_server.service
systemctl start pbs_server.service

# install torque MOMs on compute nodes
make packages

# configure compute nodes
# run torque comput node playbook to install moms and start pbs services on
# compute nodes.  The playbook has hardcoded the headnode name, so may need to edit it
ansible-playbook ../step02-torque-compute-node-playbook.yml


# configure torque on headnode
#headnode=$(printf "%s-%02d" ${base_hostname} 1)
#echo ${headnode} > /var/spool/torque/server_name
#echo "/usr/local/lib" > /etc/ld.so.conf.d/torque.conf
#linda_lib=$(printf "%s/g09/linda8.2/opteron-linux/lib" ${g09root})
#echo ${linda_lib} > /etc/ld.so.conf.d/gaussian.conf
#ldconfig

# start trqauthd daemon
systemctl enable trqauthd.service
systemctl start trqauthd.service


# configure the scheduler
cp contrib/systemd/pbs_sched.service /usr/lib/systemd/system/pbs_sched.service
systemctl enable pbs_sched.service
systemctl start pbs_sched.service

systemctl restart pbs_server.service

# additional configuration of the queues
# have the queue manager auto detect the number of processors so if we forget to set correctly it does the right thing
qmgr -c 'set server auto_node_np = True'

# set default queue wall time to 7 days (7*24*60*60) seconds
qmgr -c "set queue batch resources_default.walltime=604800"

# we might want to set a resource limit on the memory size of the compute nodes in the batch system
qmgr -c "set queue batch resources_max.mem=8gb"

# some commands to verify basic things after install
# qstat -q
# qmgr -c 'p s'
# pbsnodes -a
# qstat -Qf batch

# a basic test of submitting to the queue

# if we want to create another queue that will only start jobs on specific nodes, I think
# the following is the way to do it (haven't tried this yet).

# qmgr reference manual: http://docs.adaptivecomputing.com/torque/6-1-2/adminGuide/torque.htm#topics/torque/commands/qmgr.htm%3FTocPath%3DAppendices%7CAppendix%2520A%253A%2520%25C2%25A0Commands%2520Overview%7C_____13

# qmgr attributes: http://docs.adaptivecomputing.com/torque/6-1-2/adminGuide/torque.htm#topics/torque/13-appendices/nQueueAttributes/queueAttribReference.htm?Highlight=qmgr

# 1 add an attribute/feature to the nodes that have that required resource.  For example, we might
# use attribute bigmem for all nodes with bigger memories.
# $ cat /var/spool/torque/server_priv/nodes
# torque/pbs compute nodes
# gauss-02 np=16
# gauss-03 np=16
# gauss-04 np=16
# gauss-05 np=16
# gauss-06 np=16
# gauss-07 np=16 bigmem
# gauss-08 np=16 bigmem
# gauss-09 np=16 bigmem
# gauss-10 np=16 bigmem

# can add dynamically using qmgr
qmgr -c "set node gauss-10 properties = bigmem"

# create a separate queue for bigmem jobs
qmgr -c "create queue bigmem queue_type=execution"
qmgr -c "set queue bigmem started=true"
qmgr -c "set queue bigmem enabled=true"
qmgr -c "set queue bigmem resources_default.nodes=1"
qmgr -c "set queue bigmem resources_default.walltime=604800"
qmgr -c "set queue bigmem resources_default.mem=11gb"
qmgr -c "set queue bigmem resources_max.mem=13gb"
qmgr -c "set queue bigmem features_required=bigmem"
