#!/bin/bash
# Set up a up an example compute cluster with Gaussian 09 computational chemistry package installed.
# This is such an old version that it doesn't appear supported anymore officially by Gaussian, we
# should look to upgrading to Gaussian 16 if we can.
#
# Gaussian can be configured to work on a compute cluster, and it works with basically the same configuration
# as a standard openmpi cluster with a shared home directory and ssh key authentication set up between
# hosts within the cluster.  I don't think it actually uses mpi, but we can use the same provisioned stack.
# Thus to use this bootstrap, first provision and bootstrap a standard openmpi-compute-cluster of an
# appropriate size.  Then you should be able to run this bootstrap file to configure gaussian.
#
# Gaussian 09 is a licensed product.  We have a CD rom with the source code.  You need get a copy of the
# tared ang gzipped source file of gaussian from the cdrom to the compute cluster controller.  We should only
# need the E64-930N.tgz file from the /mnt/cdrom/tar directory of the Gaussian cdrom.  Extract that and copy
# to controller node before beginning bootstrap.
#
# Image: Centos 7 (CentOS-7-x86_64-GenericCloud.qcow2)
# Source References:
#   Here is a copy of the original Unix installation instructions: http://blog.syszone.co.kr/attachment/1581940747.pdf

# configure these variables as desired for the Gaussian compute cluster
node_count=9
base_hostname="gauss"

export mntpnt="/home/centos"
export g09root="/opt"
export g09scratch="/home/scratch"
gaussian_tarball="${mntpnt}/E64-930N.tgz"
gv_tarball="${mntpnt}/gv5.tar.bz2"
gaussian_user="gaussian"

##
# create a gaussian user and incidentially a gaussian group.  Set up the gaussian user to
# have key based authentication login using the same key currently used by the root and centos
# user.
useradd -m -d /home/${gaussian_user} -s /bin/bash ${gaussian_user}
# this creates our own keypair so we can ssh within the private network/compute cluster
mkdir -p /home/${gaussian_user}/.ssh
chown ${gaussian_user}:${gaussian_user} -R /home/${gaussian_user}/.ssh
chmod 700 /home/${gaussian_user}/.ssh
su -s /bin/bash -c "ssh-keygen -t rsa -b 2048 -P '' -f /home/${gaussian_user}/.ssh/id_rsa -C 'gaussian' " ${gaussian_user}
su -s /bin/bash -c "cat /home/${gaussian_user}/.ssh/id_rsa.pub >> /home/${gaussian_user}/.ssh/authorized_keys" ${gaussian_user}
# also need to add the public key from authorized keys into the gaussian user so we can directly ssh into
# the gaussian user account from internet, using the original keypair injected into this host
cat /home/centos/.ssh/authorized_keys | grep "Nova" >> /home/${gaussian_user}/.ssh/authorized_keys

# add the root authorized key as well
cat /root/.ssh/authorized_keys | grep "Ansible" >> /home/${gaussian_user}/.ssh/authorized_keys

chmod 600 /home/${gaussian_user}/.ssh/authorized_keys

# we will also give gaussian user sudoers access for convenience if need to perform root tasks
config="/etc/sudoers.d/${gaussian_user}"
cat > ${config} <<EOF

# add sudoers access for gaussian user
${gaussian_user} ALL=(ALL) NOPASSWD:ALL

EOF
chmod 440 ${config}


##
# extract the gaussian source/binary files for installation
cd ${g09root}
tar xvfz ${gaussian_tarball}
chown -R ${gaussian_user}:${gaussian_user} ${g09root}/g09

tar xvfj ${gv_tarball}
chown -R ${gaussian_user}:${gaussian_user} ${g09root}/gv


# perform install script
cd ${g09root}/g09
./bsd/install
# need to do again to ensure all files installed previously are changed to be owned
# by gaussian user
chown -R ${gaussian_user}:${gaussian_user} ${g09root}/g09

# we may want to configure some different scratch?
# given our current provisioning setup, there won't be much
# space available in root / directory for scratch, so lets set up
# a directory in gaussian home directory for scratch files
mkdir -p ${g09scratch}
chown -R ${gaussian_user}:${gaussian_user} ${g09scratch}


# create a local gaussian-compute-hosts.txt file for all compute nodes, 
# for gaussian parallel compute cluster
config=${g09root}/g09/gaussian-compute-hosts.txt
cat > ${config} <<EOF

# Gaussian compute cluster host names
# Nodes should be kept in the order shown for linda/gaussian execution.
# basically if you start a gaussian/linda job on the controller node 
# gauss-01, the main/master process will run on that.  The workers
# will be started from the list given here.  Also for some reason, there
# appears to be a bug in gaussian/linda, and the last host on this list
# is ignored.  We cant just give a dummy name, it does verify the hostname
# is valid.  And we can't repeat a node we already use for a worker, this
# doesn't work.  But if we add the controller node gauss-01 to the end of the
# list, linda doesn't complain, and it gets ignored and now worker is
# started there.
EOF
for ((node_num=2; node_num<=${node_count}; node_num++))
do
    printf "%s-%02d," ${base_hostname} ${node_num} >> ${config}
done
# add controller/head node to end of list of gaussian/linda hosts
printf "%s-%02d\n" ${base_hostname} 1 >> ${config}
chown ${gaussian_user}:${gaussian_user} ${g09root}/g09/gaussian-compute-hosts.txt


# create scripts to make it easier/remember how to run gaussian jobs
cp ${mntpnt}/round-floats ${g09root}/g09
chmod 754 ${g09root}/g09/round-floats
cp ${mntpnt}/g09parallel ${g09root}/g09
chmod 754 ${g09root}/g09/g09parallel
cp ${mntpnt}/g09serial ${g09root}/g09
chmod 754 ${g09root}/g09/g09serial
cp ${mntpnt}/g09qsub ${g09root}/g09
chmod 754 ${g09root}/g09/g09qsub
cp ${mntpnt}/run-all-tests ${g09root}/g09
chmod 754 ${g09root}/g09/run-all-tests

chown -R ${gaussian_user}:${gaussian_user} ${g09root}/g09

# set up gaussian tests to check install/configuration
#mkdir -p /home/${gaussian_user}/gaussian-tests
#cp ${mntpnt}/run-all-tests /home/${gaussian_user}/gaussian-tests
#chmod 744 /home/${gaussian_user}/gaussian-tests/run-all-tests
#chown -R ${gaussian_user}:${gaussian_user} /home/${gaussian_user}/gaussian-tests


# copy the password and group files for the new user to all nodes in the cluster, to ensure
# we can properly ssh to these nodes as the gaussian user
# NOTE: this playbook can be run after adding any new software package or user
# to the cluster to synch local files and users/group info to all cluster nodes
cd /root
ansible-playbook compute-cluster-copylocal-playbook.yml

