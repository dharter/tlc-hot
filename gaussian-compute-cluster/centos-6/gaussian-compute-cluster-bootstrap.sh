#!/bin/bash
# Set up a up an example compute cluster with Gaussian 09 computational chemistry package installed.
# This is such an old version that it doesn't appear supported anymore officially by Gaussian, we
# should look to upgrading to Gaussian 16 if we can.
#
# Gaussian can be configured to work on a compute cluster, and it works with basically the same configuration
# as a standard openmpi cluster with a shared home directory and ssh key authentication set up between
# hosts within the cluster.  I don't think it actually uses mpi, but we can use the same provisioned stack.
# Thus to use this bootstrap, first provision and bootstrap a standard openmpi-compute-cluster of an
# appropriate size.  Then you should be able to run this bootstrap file to configure gaussian.
#
# Gaussian 09 is a licensed product.  We have a CD rom with the source code.  You need get a copy of the
# tared ang gzipped source file of gaussian from the cdrom to the compute cluster controller.  We should only
# need the E64-930N.tgz file from the /mnt/cdrom/tar directory of the Gaussian cdrom.  Extract that and copy
# to controller node before beginning bootstrap.
#
# Image: Centos 7 (CentOS-7-x86_64-GenericCloud.qcow2)
# Source References:
#   Here is a copy of the original Unix installation instructions: http://blog.syszone.co.kr/attachment/1581940747.pdf

# configure these variables as desired for the Gaussian compute cluster
export mntpnt="/home/centos"
gaussian_tarball="${mntpnt}/E64-930N.tgz"
gv_tarball="${mntpnt}/gv5.tar.bz2"
export g09root="/opt"
gaussian_user="gaussian"
export g09scratch="/home/scratch"

##
# create a gaussian user and incidentially a gaussian group.  Set up the gaussian user to
# have key based authentication login using the same key currently used by the root and centos
# user.
useradd -m -d /home/${gaussian_user} -s /bin/bash ${gaussian_user}
# this creates our own keypair so we can ssh within the private network/compute cluster
mkdir -p /home/${gaussian_user}/.ssh
chown ${gaussian_user}:${gaussian_user} -R /home/${gaussian_user}/.ssh
chmod 700 /home/${gaussian_user}/.ssh
su -s /bin/bash -c "ssh-keygen -t rsa -b 2048 -P '' -f /home/${gaussian_user}/.ssh/id_rsa -C 'gaussian' " ${gaussian_user}
su -s /bin/bash -c "cat /home/${gaussian_user}/.ssh/id_rsa.pub >> /home/${gaussian_user}/.ssh/authorized_keys" ${gaussian_user}
# also need to add the public key from authorized keys into the gaussian user so we can directly ssh into
# the gaussian user account from internet, using the original keypair injected into this host
cat /home/centos/.ssh/authorized_keys | grep "Nova" >> /home/${gaussian_user}/.ssh/authorized_keys

# add the root authorized key as well
cat /root/.ssh/authorized_keys | grep "Ansible" >> /home/${gaussian_user}/.ssh/authorized_keys

chmod 600 /home/${gaussian_user}/.ssh/authorized_keys

# we will also give gaussian user sudoers access for convenience if need to perform root tasks
config="/etc/sudoers.d/${gaussian_user}"
cat > ${config} <<EOF

# add sudoers access for gaussian user
${gaussian_user} ALL=(ALL) NOPASSWD:ALL

EOF
chmod 440 ${config}


##
# extract the gaussian source/binary files for installation
cd ${g09root}
tar xvfz ${gaussian_tarball}
chown -R ${gaussian_user}:${gaussian_user} ${g09root}/g09

tar xvfj ${gv_tarball}
chown -R ${gaussian_user}:${gaussian_user} ${g09root}/gv


# perform install script
cd ${g09root}/g09
./bsd/install
# need to do again to ensure all files installed previously are changed to be owned
# by gaussian user
chown -R ${gaussian_user}:${gaussian_user} ${g09root}/g09

# we may want to configure some different scratch?
# given our current provisioning setup, there won't be much
# space available in root / directory for scratch, so lets set up
# a directory in gaussian home directory for scratch files
mkdir -p ${g09scratch}
chown -R ${gaussian_user}:${gaussian_user} ${g09scratch}

# set up needed user environment variables to be available on login
config="/home/${gaussian_user}/.bashrc"
cp --backup=numbered ${config} ${config}.orig
cat >> ${config} <<EOF

# usefel aliases
alias d='ls -alh'
alias rcompute='ansible gaussian_nodes -a'
alias rall='ansible gaussian_cluster -a'
alias qstat='qstat -n -l -a -1'

# set up environment variables needed by gaussian process to run
# and source the g09.profile file for additional settings for gaussian.
export g09root="${g09root}"
export GAUSS_SCRDIR="${g09scratch}"
export GAUSS_LFLAGS=' -vv -opt "Tsnet.Node.lindarsharg: ssh"'
source ${g09root}/g09/bsd/g09.profile
EOF

# create scripts to make it easier/remember how to run gaussian jobs
cp /root/gaussian-compute-hosts.txt ${g09root}/g09
chown ${gaussian_user}:${gaussian_user} ${g09root}/g09/gaussian-compute-hosts.txt

cp ${mntpnt}/round-floats.py ${g09root}/g09
chmod 744 ${g09root}/g09/round-floats.py
cp ${mntpnt}/run-gaussian-parallel ${g09root}/g09
chmod 744 ${g09root}/g09/run-gaussian-parallel
cp ${mntpnt}/run-gaussian-serial ${g09root}/g09
chmod 744 ${g09root}/g09/run-gaussian-serial

chown -R ${gaussian_user}:${gaussian_user} ${g09root}/g09

# set up gaussian tests to check install/configuration
mkdir -p /home/${gaussian_user}/gaussian-tests
cp ${mntpnt}/run-all-tests /home/${gaussian_user}/gaussian-tests
chmod 744 /home/${gaussian_user}/gaussian-tests/run-all-tests
chown -R ${gaussian_user}:${gaussian_user} /home/${gaussian_user}/gaussian-tests


# copy the password and group files for the new user to all nodes in the cluster, to ensure
# we can properly ssh to these nodes as the gaussian user
cd /root
ansible-playbook gaussian-compute-cluster-copyuser-playbook.yml


# TODO: this needs to be done for both root and gaussian .bashrc users, and the ldconfig file copied to all
# hosts.
# fix LD_LIBRARY_PATH and ldconfig
export LD_LIBRARY_PATH=/opt/g09/bsd:/opt/g09/local:/opt/g09/extras:/opt/g09:/usr/lib64/openmpi/lib:/opt/g09/linda8.2/opteron-linux/lib:/opt/gv/lib

config="/etc/ld.so.conf.d/gaussian.conf"
cat > ${config} <<EOF
/opt/g09/bsd
/opt/g09/local
/opt/g09/extras
/opt/g09
/opt/g09/linda8.2/opteron-linux/lib/
/opt/gv/lib/MesaGL
/opt/gv/lib
EOF
ldconfig
