#!/bin/bash
# General configuration to set up tunnels for services for a newly created
# vm instance/server in TheLionCloud openstack
#
# This script tested on Ubuntu server 18.04 and 19.04 versions and relies
# on presence of newer systemd services and that the ssh service is on
# and running by default.

# configure these parameters, use 0 for port number to not set up
# that tunnel type on configured server
tunnel_ip='45.33.118.94'
ssh_port='0'
http_port='0'
https_port='0'
vnc_port='0'
rdp_port='10225'
jupyterhub_port='0'
rstudio_port='0'
my_netdev='eth0'

# install needed packages
echo "Installing autossh packages..."
yum install -y autossh


# create vmtun identification key so we can communicate with tunnel
# server machine to create tunnels
echo "Creating tunnel identity files..."
cat > /root/.ssh/vmtun.pem <<EOF
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEA1Y4nnfyolE8RFdM58y+CBjqx2L5bzsXnWZmnatwrHaPIfmrR
99usHyqPB6oBP37iiEfqLuFSPLyaGA8h4Vt/uBSEdTaJtsG2wQkIsSZ29T4KD2XD
730YZOQOPEvZ7vDgOHLdfxEPvLneYyROWw3auax8QYCXRPqweHZCVwgFO4YrE8G/
h6kqcMsEBBv5gYk5+HGuwvRMV1wBdXeqd5Zmt/zbrfpYbIppvF45zySgP31inL/t
v5rWmrnV9EQKstouKNbSQ4e2m/bjwN5XOzNth5tXo/9Ffbx97l99RVjb6t0yC8lf
eNHvQeJXEaMiQ0siH0XT/MW694/+p6FHiCJvHQIDAQABAoIBABBy+qo4o+OIk5BS
zMvmhOVl7/z57KhopgFIzJR2kPufefoC0Fl/GirsZEf62E3DwD8vCuLBiEwVWukv
gTSSj6vmKK2+nazuDt2fjIPYqqEJPk0hUY5I3HApAM+0J/03oI24i0cWUVgAGRkZ
2s84qfhTSN7iwMQXazBcK4/4li6XawDvB/srDexMqPT1CSzwFJwTJulZyzuOYh/G
K0L2bxuyZiq0Jkk382mWUuOJYvoJf0jJ2tA+3KSMMTIx58WR9HH7xMI7JK86SXZ5
lmTUFbIIimh2CirljYuvCZowy86IyZ2FsYjt4+idh3hBtfPVJ6dBhNwto/M9Xwh/
e0pheCECgYEA8MAqi/u/r8WjhpngsQ+/9LV/2QrpZkstcdQZp+rLSlF1FvqaxslU
3lK5nLjxa1x09aEFRYIOgKAJ+c13b9LWOdNwx48HHqZyop7zyjSOqa7YgaLouO8H
cut9PAnq+pUwg+5OTNfIl4xg1Qs5JEa3JAhVSY+03sbThWoPRZqCemsCgYEA4xUC
QtoPJ4gh8vzK/1FEHuobeJWFmElchfyi025HjrOJs87YA15UJLD1hxWT1v7MuFQd
d3MELTxebXnPwDn+k/tSOd5LgE7S7Rzkn8TLLdTLIWoknYyxXoYZkX/fLGyRFafc
fWY9Bwyo22R6T6hwhPc8MukVuNlxTYhLoN7HLpcCgYA3XQmrvU38fue3CWXpceTD
woVPBTpdQsaFma/4tH7hbVAO47x6IEx6/+0dRE6IUO+4mLncnSb2n1cL8hsYRRTs
oVobGiIaA07zZLhNdL6l+MZjMXJxLM66t7rJ9sHptCs++6tX8nt3llmiRH10gSqW
h7ANRZXjcTetQ5GVgQ2mVwKBgHcnCuyxJsqDb+zkIFAgADKIfKmlK1hhBh94FKgE
cZ+7LUIgAtChql0mTW74L37ca+c8m8mVnFdwGcwv4obm52IWuWqL7eoBckPgqUX8
KPB/e6ao31WcAWWrz7a89gmFXguS7OsPvm34gpG8A0HRDFl4sSm1jCRWVFFuduCO
hM05AoGBAI/PED2lOVfUM32g8T1Y2VZsaDOUfxwFZiPAi5Xyos+39RoB9FFqKKKk
OWbIVuKObZ3VAkANfbsAKIuKN3H06HQf0Jv7+8q4yH0zuxWyokdGu2/+bGfTR5we
dYhAB6O5S9EgfNQm+w0sDoCblp+gVU+KLOrtuOqiBNnKqdMD+5Xb
-----END RSA PRIVATE KEY-----
EOF

# ssh won't allow identity files with too permissive permissions
chmod go-rwx /root/.ssh/vmtun.pem


# create the systemd service script
# NOTE: the quote aroudn "EOF" is important here, it suppresses variable expansion
# in the here document that follows, which is what we need here.
echo "Creating tunnel systemd services..."
cat > /usr/local/bin/vmtun.sh <<"EOF"
#!/bin/bash 
### BEGIN INIT INFO
# Provides:       vmtun
# Required-Start: $all
# Required-Stop:
# Default-Start:  2 3 4 5
# Default-Stop:
# Short-Description: set up ssh bridge to lion server
### END INIT INFO

# The network device and ip addresses we need for the tunnel
EOF

cat >> /usr/local/bin/vmtun.sh <<EOF
my_netdev='${my_netdev}'
EOF

cat >> /usr/local/bin/vmtun.sh <<"EOF"
my_ip=`/sbin/ifconfig ${my_netdev} | grep 'inet ' | awk '{print $2}'`
EOF

# we turn back on variable expansion, so we use parameter variables
# set at top of script
cat >> /usr/local/bin/vmtun.sh <<EOF
tunnel_ip='${tunnel_ip}'

# port numbers to tunnel services to, use '0' to disable tunnel for
# that services
ssh_port='${ssh_port}'
http_port='${http_port}'
https_port='${https_port}'
vnc_port='${vnc_port}'
rdp_port='${rdp_port}'
jupyterhub_port='${jupyterhub_port}'
rstudio_port='${rstudio_port}'

EOF

cat >> /usr/local/bin/vmtun.sh <<"EOF"
function d_start ( ) 
{
    echo  "vmtun: starting tunnels from: $my_ip to: ${tunnel_ip}"
    
    autossh_cmd='/usr/bin/autossh -i /root/.ssh/vmtun.pem -oStrictHostKeyChecking=false -oServerAliveInterval=30 -oServerAliveCountMax=3 -M 0 -N -v -R '
    tunnel_credentials="root@${tunnel_ip}"

    # ssh tunnel
    if [ ${ssh_port} -gt 1000 ]
    then
       ${autossh_cmd} ${tunnel_ip}:${ssh_port}:${my_ip}:22 ${tunnel_credentials} &
    fi
    
    # http tunnel
    if [ ${http_port} -gt 1000 ]
    then
       ${autossh_cmd} ${tunnel_ip}:${http_port}:${my_ip}:80 ${tunnel_credentials} &
    fi
    
    # https tunnel
    if [ ${https_port} -gt 1000 ]
    then
       ${autossh_cmd} ${tunnel_ip}:${https_port}:${my_ip}:443 ${tunnel_credentials} &
    fi
    
    # vnc tunnel
    if [ ${vnc_port} -gt 1000 ]
    then
       ${autossh_cmd} ${tunnel_ip}:${vnc_port}:${my_ip}:5901 ${tunnel_credentials} &
    fi
    
    # rdp tunnel
    if [ ${rdp_port} -gt 1000 ]
    then
       ${autossh_cmd} ${tunnel_ip}:${rdp_port}:${my_ip}:3389 ${tunnel_credentials} &
    fi
    
    # jupyterhub tunnel
    if [ ${jupyterhub_port} -gt 1000 ]
    then
       ${autossh_cmd} ${tunnel_ip}:${jupyterhub_port}:${my_ip}:8000 ${tunnel_credentials} &
    fi
    
    # rstudio server tunnel
    if [ ${rstudio_port} -gt 1000 ]
    then
       ${autossh_cmd} ${tunnel_ip}:${rstudio_port}:${my_ip}:8787 ${tunnel_credentials} &
    fi
    
    echo $!>/var/run/vmtun.pid
    echo  "PID is $(cat /var/run/vmtun.pid)" 
}
 
function d_stop ( ) 
{ 
    echo  "vmtun: stopping Service (PID = $ (cat /var/run/vmtun.pid) )" 
    kill `cat /var/run/vmtun.pid` 
    rm  /var/run/vmtun.pid
 }
 
function d_status ( ) 
{ 
    if [ -e /var/run/vmtun.pid ]; then
	echo vmtun.sh is running, pid=`cat /var/run/vmtun.pid`
	echo "tunnel ip: ${tunnel_ip}"
	echo "        ssh port: ${ssh_port}"
	echo "       http port: ${http_port}"
	echo "      https port: ${https_port}"
	echo "        vnc port: ${vnc_port}"
	echo "        rdp port: ${rdp_port}"
	echo " jupyterhub port: ${jupyterhub_port}"
	echo "    rstudio port: ${rstudio_port}"
    else
	echo vmtun.sh is NOT running
	exit 1
    fi
}
 
# Management instructions of the service 
case  "$1"  in 
    start)
	d_start
	;; 
    stop)
	d_stop
	;; 
    reload)
	d_stop
	sleep  1
	d_start
	;; 
    status)
	d_status
	;; 
    *) 
	echo  "Usage: $ 0 {start | stop | reload | status}" 
	exit  1 
	;; 
esac
 
exit  0
EOF

# needs to be an executable shell script
chmod u+x /usr/local/bin/vmtun.sh


# create the systemd config file to add the service
cat > /etc/systemd/system/vmtun.service <<EOF
[Unit]
Description = vmtun daemon
After = network.target

[Service]
Type = forking
ExecStart = /usr/local/bin/vmtun.sh start
ExecStop = /usr/local/bin/vmtun.sh stop
ExecReload = /usr/local/bin/vmtun.sh reload

[Install]
WantedBy = multi-user.target
EOF

# enable the service to start on boot and start it
echo "Starting vmtun tunnel services..."
systemctl enable vmtun
systemctl start vmtun
/usr/local/bin/vmtun.sh status


# allow password entry for sshd server and restart service to get new configuration
# if it is set to no, make it yes
#echo "Allowing password authentication via ssh (for sftp)..."
#sed -i 's/^PasswordAuthentication no$/PasswordAuthentication yes/g' /etc/ssh/sshd_config
#systemctl restart ssh

# change password for default user
#echo -e "ubuntu\nubuntu" | passwd ubuntu > /dev/null 2>&1
