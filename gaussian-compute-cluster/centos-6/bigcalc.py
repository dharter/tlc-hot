#!/usr/bin/env python
import random

if __name__ == "__main__":
    # determine a random number of iterations to perform
    num_iter = random.randint(5, 25)
    print "BigCalc entered, performing ", num_iter, " round of calculations"

    num_iter *= 10**3
    for iter in range(num_iter):
        # do some calculations
        A = []
        B = []
        C = []
        for calc in range(1000):
            A.append(random.randint(-10, 10))
            B.append(random.randint(-10, 10))
            C.append(A[-1] * B[-1])

        sum = 0
        for calc in range(1000):
            sum = sum + C[calc]
            
        # show progress
        if iter % 1000 == 0:
            print "    Finished iteration", iter
