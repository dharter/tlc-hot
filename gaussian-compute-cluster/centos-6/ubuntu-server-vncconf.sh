#!/bin/bash
# Set up standard vnc server to serve a lightweight linux desktop.
# 
# This script tested on Ubuntu server 18.04 and 19.04 versions and relies
# on presence of newer systemd services and that the ssh service is on
# and running by default.

# configure these variables as desired for the vncserver password access
vnc_user='gaussian'
vnc_user_passwd='gaussian'
vnc_port='5901'

# update and upgrade all current packages
yum update
yum -y upgrade

# install needed desktop and vnc packages
echo "Installing xfce desktop and vncserver..."
yum -y install @xfce-desktop
yum -y install tigervnc-server

# initialize vncserver
su -s /bin/sh -c "mkdir -p /home/${vnc_user}/.vnc" ${vnc_user}
su -s /bin/sh -c "printf '${vnc_user_passwd}\n${vnc_user_passwd}\n' | vncpasswd /home/${vnc_user}/.vnc/passwd" ${vnc_user}
chmod go-rw /home/${vnc_user}/.vnc/passwd

# xstartup file for xfce4 or lxqt
cat > /home/${vnc_user}/.vnc/xstartup <<"EOF"
#!/bin/sh
/usr/bin/xrdb $HOME/.Xresources
/usr/bin/startxfce4 &
#/usr/bin/startlxqt &
EOF

# xstartup file for gnome-desktop
#cat > /home/${vnc_user}/.vnc/xstartup <<"EOF"
##!/bin/sh
## Start Gnome 3 Desktop 
#[ -x /etc/vnc/xstartup ] && exec /etc/vnc/xstartup
#[ -r $HOME/.Xresources ] && xrdb $HOME/.Xresources
#vncconfig -iconic &
#dbus-launch --exit-with-session gnome-session &
#EOF

chown ${vnc_user}:${vnc_user} /home/${vnc_user}/.vnc/xstartup
chmod +x /home/${vnc_user}/.vnc/xstartup

