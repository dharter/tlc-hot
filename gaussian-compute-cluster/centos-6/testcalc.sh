#PBS -N job-testcalc
#PBS -d /home/gaussian/tmp
#PBS -l nodes=2:ppn=8,mem=8gb 
#PBS -j oe
echo "Beginning job: `date`"
echo "Started on host: `hostname`"
echo "PBS_O_HOST: ${PBS_O_HOST}"
echo "PBS_O_WORKDIR: ${PBS_O_WORKDIR}"
echo "PBS_NODEFILE: ${PBS_NODEFILE}"
cat ${PBS_NODEFILE}
./bigcalc.py
echo "Ending job: `date`"
