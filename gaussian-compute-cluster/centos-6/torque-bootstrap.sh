#/usr/bin/env bash
#
# Using these instructions: https://www.webmo.net/support/torque.html
# Official torque repository: https://github.com/adaptivecomputing/torque
# Install/admin docs: http://docs.adaptivecomputing.com/torque/6-1-1/adminGuide/help.htm

node_count=4
num_cpu=8
base_hostname="gauss"

# install required dependencies for torque
yum install -y openssl openssl-devel libxml2-devel boost-devel hwloc hwloc-devel tcl tk tcl-devel tk-devel

# build and install from repository
# clone the repository and set it up
git clone https://github.com/adaptivecomputing/torque.git -b 6.1.2 torque-6.1.2
cd torque-6.1.2
./autogen.sh

# install torque on headnode with ssh
#./configure --enable-cgroups --enable-nvidia-gpus
./configure --enable-cgroups
make
make install
source /etc/profile.d/torque.sh # add /usr/local/bin and /usr/local/sbin to path for following steps

# initialize serverdb
./torque.setup root
qterm

# node database
config=/var/spool/torque/server_priv/nodes
cat > ${config} <<EOF
# torque/pbs compute nodes
EOF
for ((node_num=1; node_num<=${node_count}; node_num++))
do
    printf "%s-%02d np=%d\n" ${base_hostname} ${node_num} ${num_cpu} >> ${config}
done

# start pbs_server
systemctl enable pbs_server.service
systemctl start pbs_server.service

# install torque MOMs on compute nodes
make packages

# configure compute nodes
# run torque comput node playbook to install moms and start pbs services on
# compute nodes.  The playbook has hardcoded the headnode name, so may need to edit it
ansible-playbook ../torque-compute-node-playbook.yml


# configure torque on headnode
#headnode=$(printf "%s-%02d" ${base_hostname} 1)
#echo ${headnode} > /var/spool/torque/server_name
#echo "/usr/local/lib" > /etc/ld.so.conf.d/torque.conf
#linda_lib=$(printf "%s/g09/linda8.2/opteron-linux/lib" ${g09root})
#echo ${linda_lib} > /etc/ld.so.conf.d/gaussian.conf
#ldconfig

# start trqauthd daemon
systemctl enable trqauthd.service
systemctl start trqauthd.service


# configure the scheduler
cp contrib/systemd/pbs_sched.service /usr/lib/systemd/system/pbs_sched.service
systemctl enable pbs_sched.service
systemctl start pbs_sched.service

systemctl restart pbs_server.service

# additional configuration of the queues
# have the queue manager auto detect the number of processors so if we forget to set correctly it does the right thing
qmgr -c 'set server auto_node_np = True'
