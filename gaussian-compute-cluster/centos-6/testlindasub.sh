#PBS -N job-gaussian09 
#PBS -d /home/gaussian/gaussian-tests
#PBS -l nodes=2:ppn=8,mem=8gb
#PBS -j oe
echo "Beginning job: `date`"
echo "Started on host: `hostname`"
echo "PBS_O_HOST: ${PBS_O_HOST}"
echo "PBS_O_WORKDIR: ${PBS_O_WORKDIR}"
echo "PBS_NODEFILE: ${PBS_NODEFILE}"
LINDA_WORKERS=`cat ${PBS_NODEFILE} | tr ' ' '\n' | sort | uniq | tr '\n' ',' | sed '{s/,$/\n/}'`
echo "LINDA_WORKERS: ${LINDA_WORKERS}"
export GAUSS_LFLAGS=' -vv -opt "Tsnet.Node.lindarsharg: ssh"'

(echo "%nprocshared=8"; echo "%lindaworkers=${LINDA_WORKERS}"; echo "%mem=8GB"; cat test043.com | grep -v "%nprocs" | grep -v "%lindaworkers" | grep -v "%mem") | \
    ${g09root}/g09/g09 > test043.log

