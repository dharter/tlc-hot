#PBS -N g09-job
#PBS -l ncpus=8,mem=8gb
#PBS -d /home/gaussian/tmp
COM_NAME=$1
BASE_NAME=`basename ${COM_NAME} .com`
LOG_NAME="${BASE_NAME}.log"

export g09root="/home/gaussian"
export GAUSS_SCRDIR="/home/gaussian/scratch"
export GAUSS_LFLAGS=' -vv -opt "Tsnet.Node.lindarsharg: ssh"'
source /home/gaussian/g09/bsd/g09.profile

echo ${LD_LIBRARY_PATH}

(echo "%nprocshared=8"; echo "%mem=8GB"; cat ${COM_NAME} | grep -v "%nprocs" | grep -v "%lindaworkers" | grep -v "%mem") | \
    /home/gaussian/g09/g09 > ${LOG_NAME}

