#!/bin/bash
#
# Install Gnome GUIi on Centos 7: https://www.itzgeek.com/how-tos/linux/centos-how-tos/install-gnome-gui-on-centos-7-rhel-7.html
# https://draculaservers.com/tutorials/install-xrdp-centos/
#
# Having lots of problems getting xrdp server to work.  The following finally seemed to fix it:
# https://www.nakivo.com/blog/how-to-use-remote-desktop-connection-ubuntu-linux-walkthrough/

# use group to install, could install Mate or Cinnamon instead
# yum group list
yum groupinstall -y "GNOME Desktop" "Graphical Administration Tools" "Server with GUI"

# could set desktop as default runlevel if want to access through spice console
# systemctl set-default graphical.target

# set up rdp server
yum install -y xrdp
systemctl enable xrdp
systemctl start xrdp

# need to add in vmtun port forwarding.  rdp serves on port 3389

# set up guacamole server
yum install -y cairo-devel libjpeg-devel libpng-devel uuid-devel freerdp-devel pango-devel libssh2-devel libssh-dev tomcat libvncserver-devel libtelnet-devel tomcat-admin-webapps tomcat-webapps gcc terminus-fonts

# get guacamole server source
wget https://downloads.apache.org/guacamole/1.2.0/source/guacamole-server-1.2.0.tar.gz

# configure and build
tar xvfz guacamole-server-1.2.0.tar.gz
cd guacamole-server-1.2.0
./configure --with-init-dir=/etc/init.d --enable-guacenc
make
make install
ldconfig

# start and enable the guacamole server
systemctl daemon-reload
systemctl start guacd
systemctl enable guacd

# install apache tomcat9 servlet
#yum install -y tomcat9 tomcat9-admin tomcat9-common tomcat9-user

# install Guacamole client
mkdir /etc/guacamole
wget https://downloads.apache.org/guacamole/1.1.0/binary/guacamole-1.1.0.war -O /etc/guacamole/guacamole.war
ln -s /etc/guacamole/guacamole.war /var/lib/tomcat/webapps/
systemctl enable tomcat 
systemctl restart tomcat
systemctl restart guacd

mkdir /etc/guacamole/{extensions,lib}
echo "GUACAMOLE_HOME=/etc/guacamole" >> /etc/default/tomcat

cat <<EOF > /etc/guacamole/guacamole.properties
guacd-hostname: localhost
guacd-port:     4822
user-mapping:   /etc/guacamole/user-mapping.xml
auth-provider:  net.sourceforge.guacamole.net.basic.BasicFileAuthenticationProvider
EOF

ln -s /etc/guacamole /usr/share/tomcat/.guacamole

# create desktop connections file
cat <<EOF > /etc/guacamole/user-mapping.xml
<user-mapping>
        
    <!-- Per-user authentication and config information -->

    <!-- A user using md5 to hash the password
         guacadmin user and its md5 hashed password below is used to 
             login to Guacamole Web UI-->
    <authorize 
            username="gaussian"
            password="gaussian">


        <!-- Allow direct connection to ssh command line terminal -->
        <connection name="Gaussian SSH Terminal Access">
            <protocol>ssh</protocol>
            <param name="hostname">127.0.0.1</param>
            <param name="port">22</param>
            <param name="server-alive-interval">600</param>
            <param name="username">gaussian</param>
            <param name="private-key">
MIIEpAIBAAKCAQEAyzAhEAVAdz+Zhq4ZtWZ1BmNP4QlhKp9I3W8owDPAWAw/1FUX
coMA9FYGkX7xpsRQHPOdZI7C2RTTwdF2CiD9NHG4UOco/znWllFh13itzUYDenR1
59nx+sLVrY3BU7fFq20IDWT6dN8VWQaLKoAfU3eis0vsGoGCmrpMDaPdXMWXNLic
+Sx0yW5FN1+Csq5zJnER9ZlVgPcj/OiWhlUE/Wr2F0bHTxu/WgPJCLrJX/zSjT+R
nsb/Wky2CdLhlU6BY4PQAXuPHPQYmRgLdGuWGAX95Yh1hG/QjH1ZNS3+kYOTVYb8
/G3z0PYRx+YrGb+nkUYctDaTX9Mx6vA1RbVLWQIDAQABAoIBAEOmVpu64+fQKRGK
Tczzu+rQjfvi35MNTXlMVUkI8fVevrG3UwMxUtQHAFDY7yQTIIlDIxgj4APex1I0
pxRgP22Cn1dwX1n6q9OIbQcMivFZajcPJ2J2Ufanf/U0yTVSbmHQ/2lWQX0/+R+W
6ibQWZ20q1r+4DFDBXT2qfOwpzcHinniqWNK8zp++NTENrXXU3XmOXLR07XnFMRj
P4lqzQTnZTK70ASIKiheHKwGFBQe1pkdgusgdEX0yqm9ja5/RKOydnjrPWOwD618
FHuk+UeVOtbw7VlVjF8odulhEEw2H7DyTjVANkYnu2tDs+P3J2hiasHI4hpGsbaq
Ywo7TDECgYEA7gveN1/zJx4ds8zmpeQ+LDSyKmawK3CqPiouqdqINkUzxTM/9ZWH
MuKHQplUPb/JTKxkUpzYborH+iLdIqVXEESgkZ9GPFltooVCIyVchFdU2npZCQvo
4AtVSAIB91dXuGvPWutFDblI/ObaomHdxJA503MplTopCTuY6ig8VI8CgYEA2oM5
0Br6z42JIDmnPCSTvZFJNpM8nz/mp3XOSvHKkr9MDMbrJUFYToN/SCJ/drdlqJtO
RQ3P+YdgHyzV1zMEf5ZJYjKyFOBerCtBll+PmSM229ls2dY2D2Azxnd0X6yakBJJ
r/adwjXsCtkTzbVcASNhAPr9RdF2xXXN6TaNZZcCgYAhonOnh1sn781qlk0Qq4tN
W7TA4PmLacPkPLmTLlmIsYZ7Gp6y6InN2swZ0OhiQTMELmeOdLLwC5Pt9/ce6hW/
KrM1YDfgVxJyoDmUFbdYy64loJvr907UOUW3gvEcLa8rU7BcvFlpcwCpG0VRJFrd
ZzOEeQUrHKinsVAN5dmAaQKBgQCVIM/49iDMO8UlKBO5WSE3VAtKYswRxzOsgBrt
TP06j7HgWnWq9GDHHLlmSp0BMYMAwhV1zUpt+vUf8aJzfw8H4FJu4ThmRQZ3rAHU
FZ3UHpNXEZlXRfmZHsfXIDlc/lhyEPkXTVE3dUOF18bLMoX7BOE0IRdvadgMRSzl
h+SSOwKBgQDGV0BNuWbheRmdBFmBhSLOmGolAO/YRdCSUub8OUPLgV+oEL2hy8FE
nTCn6FXwxr6n/U7e4RZS4WGE+AOOM06w41PDiCGJyPvZRSGuh6fC1unXDwsETOtv
dLPbIamgRgcMDLjQhBkup6gkwucTkWaF+WMFyP56v1PZDmpHebunxg==
          </param>
          </param name="enable-sftp">true</param>
        </connection>

        <!-- Allow direct connection to xrdp server at port 3389 remote connection -->
        <connection name="Gaussian Remote Desktop Protocol (RDP) Desktop Session">
            <protocol>rdp</protocol>
            <param name="hostname">127.0.0.1</param>
            <param name="port">3389</param>
            <param name="ignore-cert">true</param>
        </connection>

        <!-- Allow direct connection to vnc desktop server at port 5900 remote connection -->
        <connection name="Gaussian VNC Desktop Session">
            <protocol>vnc</protocol>
            <param name="hostname">127.0.0.1</param>
            <param name="port">5900</param>
            <param name="ignore-cert">true</param>
        </connection>

    </authorize>

</user-mapping>
EOF

chmod 600 /etc/guacamole/user-mapping.xml
chown tomcat:tomcat /etc/guacamole/user-mapping.xml

cat <<EOF >> /etc/tomcat/catalina.properties

# added by guacamole install/setup
guacamole.home=/etc/guacamole
EOF

systemctl daemon-reload
systemctl restart guacd
systemctl restart tomcat 


# install a vnc server
yum install -y tigervnc-server
cp /lib/systemd/system/vncserver@.service  /etc/systemd/system/vncserver@:1.service
# add PIDFile line to the sysemd service
# PIDFile=/home/gaussian/.vnc/%H%i.pid

systemctl daemon-reload
systemctl start vncserver@:1
systemctl enable vncserver@:1 


# install a scraping server
yum install -y tigervnc-server
systemctl isolate graphical.target
