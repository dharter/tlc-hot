# https://github.com/frgaudet/openstack-heat-mpi/blob/master/mpi.yaml
heat_template_version: rocky
description: >
  Create a compute cluster for performing gaussian computational chemistry calculations,
  set up for parallel execution.  This stack consists of a number of compute nodes, connected through
  a private network.  The compute nodes will be configured with openmpi and a shared nfs
  directory on an external volume (eventually).

parameters:
  public_network_id:
    type: string
    label: Public Network ID
    description: Name of public (provider) network to attach floating ip of controller node to.
    default: tamuc
  key_name:
    type: string
    label: Key Name
    description: Name of key-pair to be used for compute instance identity access.
    default: tlckey
  node_count:
    type: number
    label: Gaussian Cluster Node Count
    description: The number of Gaussian compute nodes to create, includes both the controller node and all compute nodes.
    default: 4
  node_count_compute:
    type: number
    label: Gaussian Compute Node Count
    description: Number of compute nodes, this should be node_count - 1 (it is not possible to do arithmetic in HOT templates so this is a bit of a workaround, just ensure that this is set to node_count - 1).
    default: 3
  node_count_last_ip:
    type: string
    label: Last ip for allocation pool
    description: Again also a bit of a workaround.  If node_count is 4, this should be 192.168.0.4.  Whatever the node count is, set this value as 192.168.0.node_count
    default: 192.168.0.4
  node_basename:
    type: string
    label: Node base hostname
    description: Basename of hostnames for Gaussian nodes in the cluster.
    default: gauss
  volume_size:
    type: number
    label: Data volume size (Gb)
    description: Size of volume to hold /home user data in compute cluster
    default: 50
  image_id:
    type: string
    label: Image ID
    description: Image to be used for compute node instances.
    default: centos-7
  image_flavor:
    type: string
    label: Image Flavor
    description: Type of instance (flavor) to be used for compute nodes.
    default: gauss.full.disk

resources:
  # The router where network traffic will pass through.
  gauss-router:
    type: OS::Neutron::Router
    properties:
      name: gauss-router
      external_gateway_info:
        network: { get_param: public_network_id }

  # Associates a router with a subnet or a port.
  gauss-router-interface:
    type: OS::Neutron::RouterInterface
    properties:
      router: { get_resource: gauss-router }
      subnet: { get_resource: gauss-subnet }

  # The network that will contain one, or many subnets.
  gauss-net:
    type: OS::Neutron::Net
    properties:
      name: gauss-net
  
  # Subnet that the instance will be connected to and receive a ip address from.
  gauss-subnet:
    type: OS::Neutron::Subnet
    properties:
      cidr: 192.168.0.0/24
      # bit of a hack/kludge, basically the dhcp server is assigned first ip
      # in first allocation pool, thus we can effectively assign where dhcp
      # lands using 2 allocation pools here
      allocation_pools:
        - {start: 192.168.0.100, end: 192.168.0.100}
        - {start: 192.168.0.2, end: {get_param: node_count_last_ip}}
      dns_nameservers: [8.8.8.8, 8.8.4.4, 10.14.0.54, 10.14.0.75]
      network: { get_resource: gauss-net }
      gateway_ip: 192.168.0.101
      enable_dhcp: true

  # A Port that our controller instance will attach it's interface on to.
  gauss-controller-port:
    type: OS::Neutron::Port
    properties:
      network: { get_resource: gauss-net }
      fixed_ips:
        - ip_address: 192.168.0.1
      
  # A resource where Floating IPs will be taken from.
  gauss-controller-floatingip:
    type: OS::Neutron::FloatingIP
    properties:
      floating_network: { get_param: public_network_id }

  # Creates the association from a Floating IP to the Port
  gauss-controller-association:
    type: OS::Neutron::FloatingIPAssociation
    properties:
      floatingip_id: { get_resource: gauss-controller-floatingip }
      port_id: { get_resource: gauss-controller-port }

  # The controller node
  gauss-compute-controller:
    type: OS::Nova::Server
    properties:
      name:
        str_replace:
          template:
            $name-controller
          params:
            $name: {get_param: node_basename}
      key_name: { get_param: key_name }
      flavor: { get_param: image_flavor }
      image: { get_param: image_id }
      networks:
        - subnet: { get_resource: gauss-subnet }
          port: { get_resource: gauss-controller-port }

  # cinder volume which will hold /home user data for compute cluster
  gauss-data-volume:
    type: OS::Cinder::Volume
    properties:
      size: {get_param: volume_size}
  gauss-data-volume-attachment:
    type: OS::Cinder::VolumeAttachment
    properties:
      volume_id: {get_resource: gauss-data-volume}
      instance_uuid: {get_resource: gauss-compute-controller}
      mountpoint: /dev/vdb
      
  # the compute nodes for the cluster
  gauss-compute-nodes:
    type: OS::Heat::ResourceGroup
    properties:
      count: {get_param: node_count_compute}
      resource_def:
        type: OS::Nova::Server
        properties:
          name:
            str_replace:
              template:
                $name-$index
              params:
                $name: {get_param: node_basename}
                $index: "%index%"
          key_name: {get_param: key_name}
          flavor: {get_param: image_flavor}
          image: {get_param: image_id}
          networks:
            - subnet: {get_resource: gauss-subnet}

outputs:
  instance_ips: 
    description: The IP addresses of the deployed instance controller node and compute nodes
    value: [ { get_attr: [gauss-compute-controller, first_address]}, {get_attr: [gauss-compute-nodes, first_address]} ]
  floating_ip:
    description: The floating IP assigned for access to the deployed instance controller node
    value: { get_attr: [ gauss-controller-floatingip, floating_ip_address ]}


# fix dhcp port address
# Find the network:dhcp port, if it is not 192.168.0.100
# # openstack port show 9fb42ae8-17b8-4593-abab-5a2c4ff66d81
# # openstack port set --fixed-ip subnet=24794c47-c1ec-4ca5-a0e1-3c7ac1eb21d3,ip-address=192.168.0.100 9fb42ae8-17b8-4593-abab-5a2c4ff66d81
#
# #  openstack port set --fixed-ip subnet=24794c47-c1ec-4ca5-a0e1-3c7ac1eb21d3,ip-address=192.168.0.5 1-port-0
# # openstack port set --fixed-ip subnet=24794c47-c1ec-4ca5-a0e1-3c7ac1eb21d3,ip-address=192.168.0.100 9fb42ae8-17b8-4593-abab-5a2c4ff66d81
