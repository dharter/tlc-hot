#PBS -N job-gaussian09 
#PBS -d /home/gaussian/tmp
#PBS -l nodes=1:ppn=8,mem=8gb
#PBS -j oe
echo "Beginning job: `date`"
echo "Started on host: `hostname`"
echo "PBS_O_HOST: ${PBS_O_HOST}"
echo "PBS_O_WORKDIR: ${PBS_O_WORKDIR}"
echo "PBS_NODEFILE: ${PBS_NODEFILE}"
cat ${PBS_NODEFILE}

export g09root="/opt"
export GAUSS_SCRDIR="/home/scratch"
export GAUSS_LFLAGS=' -vv -opt "Tsnet.Node.lindarsharg: ssh"'
source /opt/g09/bsd/g09.profile
export LD_LIBRARY_PATH=/opt/g09/bsd:/opt/g09/local:/opt/g09/extras:/opt/g09:/usr/lib64/openmpi/lib:/opt/g09/linda8.2/opteron-linux/lib/:/opt/gv/lib

echo "LD_LIBRARY_PATH: ${LD_LIBRARY_PATH}"

g09 < test043.com > test043.log

echo "Ending job: `date`"
