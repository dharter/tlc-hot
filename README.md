# TAMUC Lion Cloud (TLC) Example Heat Orchestration Templates (HOT) tlc-hot

This repository contains example working OpenStack heat orchestration templates, designed to create basic useful system configuration on the [TAMUC Lion Cloud](https://lion.harter.pro:3443), an [OpenStack Cloud](https://www.openstack.org/) available for computing, research and education at [Texas A&M University - Commerce](https://www.tamuc.edu/).  If you are an educator, researcher or student at TAMU-Commerce, please contact  Dr. Derek Harter (Derek dot Harter at tamu-commerce dot edu) at the  [Department of Computer Science](https://www.tamuc.edu/cs) for information on obtaining an account and using TLC for your research or class project. 

## Getting Started

This repository contains a collection of  [HOT templates](https://docs.openstack.org/heat/rocky/template_guide/hot_guide.html) ([yaml](https://en.wikipedia.org/wiki/YAML) formatted yet another markup language), as well as shell scripts and  [Ansible playbooks](https://www.ansible.com/), designed to create basic reusable virtual server and virtual cluster configurations for common computing tasks on the TAMUC Lion Cloud at TAMU-Commerce.  For example you will find HOT templates to create a basic LAMP (Linux Apache MySql Php) server, a standard multi-cpu system for OpenMP shared memory computational tasks, a server with JupyterHub and RStudio configured for Python/R scientific computing work, and small virtual cluster configurations for creating Open MPI distributed memory parallel computing clusters in this repository. 

These templates are example, and may need tweaking or customization to work properly.  In addition, these templates were created with the specific resources of The Lion Cluster in mind, including virtual server images, default network configurations, and flavor definitions. These parameters would of course need to be customized for your own environment, though in most cases these should be specified as HOT template parameters  that can be specified when launching the stack from the HOT template. 
### Prerequisites

You will need an account on The Lion Cloud @ TAMU-Commerce, or on a similar OpenStack cloud computing service to use the resources in this repository. 

In general, each directory in the repository contains the resources to setup and configure one type of virtual computing server/system. There will usually be two basic files for each virtual server/system in each directory, where 'system-type' is the name of the server/system, like 'lamp-server' or 'openmp-shared-memory-compute': 

- system-type-hot.yml: The heat orchestration template to create the virtual   machine resources for this computing system type.  Basically creation of   virtual computing resources is broken down into 2 separate parts.  The HOT   template is the "provisioning" pieces of the process.  It provisions   resources, such as virtual servers, creates virtual networks, creates   virtual disk volumes, etc.  Basically this step in the process creates all   of the virtual machine resources needed for the computing/server system   we are creating. 

- system-type-bootstrap.sh: After provisioning system resources, we   need to actually perform "configuration management", which is a   fancy way of saying we need to install needed software and configure   the software and operating systems of the virtual hardware systems   for use.  For more complex provisioning, this repository makes use   of Ansible and Ansible playbooks to automate the configuration   management.  Ansible is an example of a   [DevOps](https://theagileadmin.com/what-is-devops/) tool, and in   general the use of virtualization and agile configuration management   and provisioning are all examples of this use of DevOps in the   industry for managing and creating computing resources for   enterprise activities.  The bootstrap shell script may be all   that is needed for simple configurations.  It will usually set   up ssh access and install needed packages for the server/system. 

In addition to the HOT template and bootstrap script, there may be one or more additional Ansible playbooks associated with a server/system configuration.  These playbooks perform more complex configurations, especially for virtual network cluster system setup. 

The typical use then of these scripts to create a particular desired server/system is as follows: 

1. From the TLC Horizon Dashboard, select Orchestration->Stacks and then +Launch Stack.    Select the desired HOT template from a file (if you have cloned this    repository to a local directory for example), or simply use Direct Input    for the template source and copy/paste the contents of the desired HOT    template into the provided text area. 
2. Go to the Next step of the HOT template to launch the stack.  Specify/modify    any needed parameters for your particular needs.  Usually the parameter defaults    should be sufficient to create a small but working example of the server/system    type on TLC.  Once you are ready hit Launch to provision the virtual resources. 
3. Once the resources are doing being provisioned and are available, use your     key based authentication to log into the server or primary controller node    created for your server/system.  You will also need to determine the public    or floating IP address that was assigned to your server or controller node    of the server/system you are creating. 
4. Once on your sustem, use sudo to become root so you can run the bootstrap    scripts and playbooks. 
5. Copy over the bootstrap shell script, and any additional scripts and ansible    playbooks used for the server/system configuration.  Make the bootstrap    script executable and execute it to configure your server/system: 

```bash
# externally, use your key and the assigned public/floating ip address to ssh
# into your server
$ ssh -i mykey.pem ubuntu@172.16.200.158

# become root to run bootstrap scripts
$ sudo su -

# copy over the bootstrap script and associated playbooks.
# you can use scp command, or you can use simple copy/paste
$ cat > system-type-bootstrap.sh
    Copy the text of the script from a file, then paste it into your terminal
    ctrl-d will then close the stream and finish writing the file
	
# make script executable and execute it
$ chmod u+x system-type-bootstrap.sh
$ ./system-type-bootstrap.sh
```


## Contributing

Please read [CONTRIBUTING.md]() for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning


## Authors

* **Derek Harter** - *Initial work* - [harter.pro](http://www.harter.pro/)

See also the list of [contributors]() who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments


