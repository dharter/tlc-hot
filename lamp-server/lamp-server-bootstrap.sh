#!/bin/bash
# Set up standard LAMP (Linux Apache MySql PHP) server stack on an
# OpenStack VM instance server.  While we are at it, we also set up
# a PhpMyAdmin service so we can use web based interface to manage the
# mysql database on the node.
#
# This script tested on Ubuntu server 18.04 and 19.04 versions and relies
# on presence of newer systemd services and that the ssh service is on
# and running by default.
# Source References:
#   https://www.linode.com/docs/web-servers/lamp/install-lamp-stack-on-ubuntu-18-04/
#   https://www.linode.com/docs/web-servers/lamp/install-lamp-stack-on-ubuntu-18-04/

# configure these variables as desired for the LAMP components password access
hostname='tlc.tamuc.edu'
database='webdata'
database_user='webuser'
database_user_pass='webuserPass'
database_root_pass='rootPass'
phpmyadmin_pass='phpmyadminPass'

# update and upgrade all current packages
apt update
DEBIAN_FRONTEND=noninteractive apt -y dist-upgrade
apt -y autoremove

# use tasksel to install all essential LAMP components at once for ubuntu server
DEBIAN_FRONTEND=noninteractive apt -y install tasksel
tasksel install lamp-server # not sure if this will work in noninteractive script?

# configure Apache
conf='/etc/apache2/apache2.conf'
cp --backup=numbered ${conf} ${conf}.orig
sed -i 's/KeepAlive Off/KeepAlive On/g' ${conf}
sed -i 's/MaxKeepAliveRequests 100/MaxKeepAliveRequests 50/g' ${conf}
sed -i 's/KeepAliveTimeout 10/KeepAliveTimeout 5/g' ${conf}

conf='/etc/apache2/mods-available/mpm_prefork.conf'
cp --backup=numbered ${conf} ${conf}.orig
sed -i 's/^[^#].*StartServers.*/\tStartServers\t\t5/g' ${conf}
sed -i 's/^[^#].*MinSpareServers.*/\tMinSpareServers\t\t3/g' ${conf}
sed -i 's/^[^#].*MaxSpareServers.*/\tMaxSpareServers\t\t40/g' ${conf}
sed -i 's/^[^#].*MaxRequestWorkers.*/\tMaxRequestWorkers\t200/g' ${conf}
sed -i 's/^[^#].*MaxConnectionsPerChild.*/\tMaxConnectionsPerChild\t10000/g' ${conf}

# disable event module and enable prefork
a2dismod mpm_event
a2enmod mpm_prefork

# restart apache to pick up config changes
systemctl restart apache2


# configure example virtual hosts
conf="/etc/apache2/sites-available/${hostname}.conf"
cp /etc/apache2/sites-available/000-default.conf ${conf}

sed -i "1i <Directory /var/www/html/${hostname}/public_html>\n\tRequire all granted\n</Directory>\n" ${conf}
sed -i "s|.*#ServerName .*|\tServerName ${hostname}\n\tServerAlias www.${hostname}|g" ${conf}
sed -i "s|.*DocumentRoot .*|\tDocumentRoot /var/www/html/${hostname}/public_html|g" ${conf}
sed -i "s|.*ErrorLog .*|\tErrorLog /var/www/html/${hostname}/logs/error.log|g" ${conf}
sed -i "s|.*CustomLog .*|\tCustomLog /var/www/html/${hostname}/logs/access.log combined|g" ${conf}

# and create the directories we referenced above for use for the virtual hosts
mkdir -p /var/www/html/${hostname}/{public_html,logs}
# do we need to set permissions for this directory?

# link virtual host file from sites-available to sites-enabled
a2ensite ${hostname}

# disable default virtual host to minimize security risk
a2dissite 000-default.conf

# reload apache to get new config
systemctl reload apache2


# configure MySQL
# create an example database and add an example user
mysql -u root <<EOF
CREATE DATABASE ${database};
GRANT ALL ON ${database}.* TO "${database_user}"@"localhost" IDENTIFIED BY "${database_user_pass}";
EOF


# this probably can't be done here, can try using file input
# 1: set up validate password plugin: y
# 2: password policy level: 0=LOW, 1=MEDIUM, 2=STRONG
# 3,4: set root password
# 5: Estimated strength of password, do you wish to continue with this password: y
# 6: Remove anonymous users: y
# 7: disallow root login remotely: y
# 8: remove test database and access to it: y
# 9: reload privilege tables now: y
#mysql_secure_installation <<EOF
#y
#0
#${database_root_pass}
#${database_root_pass}
#y
#y
#y
#y
#y
#EOF
# this doesn't work using i/o redirection, need to do the mysql_secure manually.  I believe these
# are all of the steps that are done by the secure script:
# https://stackoverflow.com/questions/24270733/automate-mysql-secure-installation-with-echo-command-via-a-shell-script
# set root password
mysql -e "UPDATE mysql.user SET authentication_string = PASSWORD('${database_root_pass}') WHERE User = 'root';"
# Remove the anonymous users
mysql -e "DROP USER ''@'localhost';"
mysql -e "DELETE FROM mysql.user where User='';"
# remove remote root access
mysql -e "DELETE FROM mysql.user WHERE User='root' and Host NOT IN ('localhost', '127.0.0.1', '::1');"
# drop anonymous user from hostname access
mysql -e "DROP USER ''@'$(hostname)'"
# drop the test database
mysql -e "DROP DATABASE test"
# make these changes take immediate effect
mysql -e "FLUSH PRIVILEGES"


# configure PHP settings
# Edit configureation in /etc/php/7.2/apache2/php.ini for more descriptive
# errors, logging an dperformance
conf='/etc/php/7.2/apache2/php.ini'
cp --backup=numbered ${conf} ${conf}.orig
sed -i 's/^error_reporting = E_ALL & ~E_DEPRECATED & ~E_STRICT/error_reporting = E_COMPILE_ERROR | E_RECOVERABLE_ERROR | E_ERROR | E_CORE_ERROR/g' ${conf}
sed -i 's/^max_input_time = 60/max_input_time = 30/g' ${conf}
sed -i 's|^;error_log = syslog|error_log = /var/log/php/error.log|g' ${conf}

mkdir /var/log/php/
chown www-data /var/log/php

systemctl restart apache2

# if we want to use WordPress, need some additional modules
# apt install php-curl php-gd php-mbstring php-xml php-xmlrpc

# create a test php file so we can see if apache2 is serving the virtual
# host and get php information
conf="/var/www/html/${hostname}/public_html/phptest.php"
cat > ${conf} <<"EOF"
<html>
<head>
    <title>PHP Test</title>
</head>
    <body>
    <?php echo '<p>Hello World</p>';

    // In the variables section below, replace user and password with your own MySQL credentials as created on your server
    $servername = "localhost";
EOF
cat >> ${conf} <<EOF
    \$username = "${database_user}";
    \$password = "${database_user_pass}";
EOF
cat >> ${conf} <<"EOF"

    // Create MySQL connection
    $conn = mysqli_connect($servername, $username, $password);

    // Check connection - if it fails, output will include the error message
    if (!$conn) {
        die('<p>Connection failed: <p>' . mysqli_connect_error());
    }
    echo '<p>Connected successfully</p>';
    ?>
</body>
</html>

EOF


# another test, get some php info
conf="/var/www/html/${hostname}/public_html/phpinfo.php"
echo "<?php phpinfo(); ?>" > ${conf}

# install PhpMyAdmin
# not sure if this will work noninteractively, needs user input...
apt -y install phpmyadmin
# selected option to configure with dbconfig-common options first time...
#phpmyadmin password: phpmyadminPass

# restart apache2 to get config changes
systemctl restart apache2


# set up https using a self-signed TLS certificate
# create the self signed certificate and private key
cert_conf="/etc/ssl/certs/${hostname}.crt"
key_conf="/etc/ssl/private/${hostname}.key"
ca_conf="/etc/ssl/certs/ca-certificates.crt"

# this is interactive, can we add options to answer these questions?
# 1: country name: US
# 2: state: Texas
# 3: locality (city): Commerce
# 4: organiztion: TAMUC
# 5: organization unit name: Dept CS
# 6: common name: ${hostname} (tstlamp.tamuc.edu)
# 7: email address: derek.harter@tamuc.edu
openssl req -new -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -out ${cert_conf} -keyout ${key_conf} <<EOF
US
Texas
Commerce
TAMUC
Dept CS
${hostname}
derek.harter@tamuc.edu
EOF

# set access permissions as needed (read only for user)
chmod 400 ${key_conf}

# append configuration for VirtualHost https using the signed certificates
conf="/etc/apache2/sites-available/${hostname}.conf"
cat >> ${conf} <<EOF

<VirtualHost *:443>
    SSLEngine On
    SSLCertificateFile ${cert_conf}
    SSLCertificateKeyFile ${key_conf}
    SSLCACertificateFile ${ca_conf}

    ServerAdmin first.last@tamuc.edu
    ServerName ${hostname}
    ServerAlias www.${hostname}
    DocumentRoot /var/www/html/${hostname}/public_html/
    ErrorLog /var/www/html/${hostname}/logs/error.log
    CustomLog /var/www/html/${hostname}/logs/access.log combined
</VirtualHost>
EOF

# ensure apache ssl module is enabled and enable the virtual host config
a2enmod ssl
a2ensite ${hostname}

systemctl restart apache2
