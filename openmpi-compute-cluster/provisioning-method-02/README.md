# Open MPI Compute Cluster

This is an example of provisioning and configuring a small computer cluster suitable for running Open MPI distributed memory compute tasks.  This cluster creates a number of compute nodes and a data volume.  A special controller node is designated as the controller or head node.  The controller node mounts the external volume to be used for /home data collection for the cluster.  We set up NFS file sharing so that all compute nodes actually remove mount and share their home directory from this external volume.  This cluster configures open key based ssh authentication for both the root user and the ubuntu user of the cluster (assuming default ubuntu user for an install from a ubuntu OS server distribution).  OpenMPI expects/requires key based (passwordless) authentication between all nodes that will participate in the Open MPI tasks to be run.

## Provisioning Method 02

There were some disadvantages/anoyances to the first method we created for provisioning a compute cluster. Ideally we would like to specify the num_nodes parameter and this will be the actual number of total compute  nodes (including controller) in the cluster.  And we would like for ip addresses to be mapped in a predictable way.  So for example, in method 02 we are trying to end up with the following mapping.  Given num_nodes is 4, we want:

- mpi-compute-01: 192.168.0.1 (this will also act as the controller/head node and will have storage volume and floating ip attached to it).
- mpi-compute-02: 192.168.0.2
- mpi-compute-03: 192.168.0.3
- mpi-compute-04: 192.168.0.4

We want the dhcp server to be at the standard x.x.x.1 address, so we will set it up to be running at 192.168.0.1, and we will use a subnetwork cidr of 9 bits 192.168.0.0/23, so that 192.168.0.x and 192.168.1.x addresses are on the same subnetwork mask.

In order to get the ip mapping and host names we want, we are going to need to do a few kludgy tricks, because HOT templates don't really allow you to specify a static mapping of ip addresses you want when creating a resource group, nor does it allow you to easily use a slightly different format for the host names or modify the index to begin counting at 1 instead of 0.  We thus specify a restricted range of ip addresses, so that we can predict ahead of time which ip addresses will be used.  So again for num_nodes=4, we will specify in the HOT template to use begin and end ranges of the ip address such as begin=192.168.1.1,end=192.168.1.4.  This will guarantee that addresses 1 through 4 are used for the 4 created server resources.  However, openstack will randomly map its resource name to an assigned ip, thus mpi-compute-0 might end up associated with 192.168.1.3, mpi-compute-1 with 192.168.1.2, or whatever.  When we setup configuration, we will be changing the hostname to match the mapping we desire, ignoring the resource name given by openstack.  So this should work to get the desired effect, with one downside being that the node openstack considers mpi-compute-0 might end up getting mapped to ip 192.168.1.3 and thus we will rename it as mpi-compute-03.  But this annoyance doesn't really effect anything from the  point of view of the openmpi compute software, as it is only the hostname, /etc/hosts and hostfile that matter in setting up and running open mpi tasks.  For this minor problem we fix most of the other issues we had with method 01.  We will have an expected mapping of hostnames to ip addresses, and we will number hostnames starting at 01.  Also we can avoid having to copy the ip addresses that were provisioned to the bootstrap script, as we now know which ip addresses are to be used.  We only need to set parameters for the ip address range and/or begin ip address and number of nodes in the cluster for our bootstrap. 

### Example Usage

1. Navigate to Orchestration->Stacks in Horizon and perform + Launch Stack
2. Choose the openmpi-compute-cluster-hot.yml heat template for stack launch.  Fill in the stack name and password.  Choose the correct keypair you intend to use for access to this cluster, or create a new keypair first if needed.  Determine the number of nodes you need in the cluster.  Set node_count to this number and also specify node_count_computer as 1 less than this, and set the node_count_last_ip address to your desired node count.  For example, if you want an 8 node cluster, node_count should be 8, node_count_compute should be set to 7, and the node_count_last_ip should be 192.168.0.8
3. Once stack finishes provisioning, you need to copy over the bootstrap file, ansible file, and your private key pair which you need to access all of the compute nodes created on the private network for the cluster.  Assuming that these file reside in a directory and you are on a local machine that has ssh access to the TLC cloud instances.  First of all find the floating ip address assigned to the controller node.  In the next example, the controller node has a public floating ip address of 172.16.202.34.  Then the files you need to copy should be in a directory, such as for example src_dir="~/repos/tlc-hot/openmpi-compute-cluster/provisioning-method-02".  You can copy the files you need using for example scp command

        user@local:~$ src_dir="~/repos/tlc-hot/openmpi-compute-cluster/provisioning-method-02"
        user@local:~$ public_ip="172.16.202.34"
        user@local:~$ scp -i tlckey.pem ${src_dir}/tlckey.pem ubuntu@${public_ip}:/tmp
        user@local:~$ scp -i tlckey.pem ${src_dir}/openmpi-compute-cluster-bootstrap.sh ubuntu@${public_ip}:/tmp
        user@local:~$ scp -i tlckey.pem ${src_dir}/openmpi-compute-cluster-playbook.yml ubuntu@${public_ip}:/tmp

  In this example we copied our private key, and the bootstrap and playbook files to the ubuntu account using the public floating ip address that was assigned to our cluster controller node.
4. Log into the controller node so you can execute the bootstrap script.  You need to log in as the ubuntu user, the use sudo to become the root user.

        user@local:~$ ssh -i tlckey.pem ubuntu@${public_ip}
        ubuntu@mpi-compute-controller:~$ sudo su -
        root@mpi-compute-controller:~# 

5. We need to edit the parameters in the bootstrap file to reflect what you chose when provisioning the cluster stack.  You could edit these before copying them onto your controller node, or edit them now.

        root@mpi-compute-controller:~# cp /tmp/openmpi-* /tmp/tlckey.pem .
        # edit necessary parameters in bootstrap file, for example make sure the node_count and key_filename are correct
        # given how you provisioned your stack
        root@mpi-compute-controller:~# cat openmpi-compute-cluster-bootstrap.sh 

        node_count=4
        base_hostname="mpi"
        data_dev="/dev/vdb"
        key_filename="tlckey.pem"

6. And finally, you should be ready to finish configuration by executing the bootstrap script.

        root@mpi-compute-controller:~# chmod u+x openmpi-compute-cluster-bootstrap.sh
        root@mpi-compute-controller:~# ./openmpi-compute-cluster-bootstrap.sh 
