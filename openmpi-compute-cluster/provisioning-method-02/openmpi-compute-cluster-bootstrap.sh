#!/bin/bash
# Set up a test/example openmpi cluster.  This script bootstraps the configuration of all
# compute nodes for the cluster compute.  This script should be run on the controller node
# (node 01) of the cluster, and it will communicate with and bootstrap the configuration
# setup for the controller and all compute nodes in the cluster.  The following
# configuration tasks are performed here:
# 1. Set up key based ssh authentication (without password) between all nodes, for both root and
#    the default ubuntu user.
# 2. Install ansible for configuration management.
# 3. Using Ansible, instlal needed openmpi and base gcc toolchain tools.
# 4. We expect a cinder volume to be created and attached to the controller node 01.  This
#    bootstrap script creates a file system, mounts the volume, and sets up appropriate nfs
#    share so that all compute node users /home directories are sharing data from this
#    common volume attached to the controller node 01.
#
# This bootstrap script requires you to copy this file and the following additional files
# before invoking the bootstrap:
# - cloudkey.pem Copy the private .pem identity file to the bootstrap directory, this was the key pair
#   you specified when you created the stack using the hot tamplate.  Name the file as shown, or else
#   change the parameter name below to the correct name for your .pem identity file.
# - openmpi-compute-cluster-playbook.yml Copy the associated ansible playbook for this
#   configuration management bootstrap process to the controller node.
#
# Source References:
#   https://support.rackspace.com/how-to/high-performance-computing-cluster-in-a-cloud-environment/
#   https://github.com/open-mpi/ompi
#   https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-ansible-on-ubuntu-18-04

# configure these variables as desired for the OpenMPI cluster
# It is necessary you correctly specify the node count, we assume compute nodes
# have been provisioned on ip addresses 192.168.0.1 through 192.168.0.node_count, and that
# the controller node was assigned 192.168.0.1 and this cript is running on the controller
# node.
node_count=4
base_hostname="mpi"
data_dev="/dev/vdb"
key_filename="tlckey.pem"
default_user="ubuntu"

##
# configure the attached volume to be new home directory
# We are going to mount over the current home directory, so we first make a copy of our
# only user to a file archive, which we will unarchive into the new volume once we
# mount it
cd /home
tar cvfj /root/home.tar.bz2 *

# create an ext4 filesystem on the device
# and mount it as /home
# TODO: should skip this if we want to mount an already existing volume with home data...
mkfs.ext4 -F ${data_dev}
echo "${data_dev}    /home    ext4    defaults 0 0" >> /etc/fstab
mount -a

# and untar the home archive to copy over any default users created when image was installed
cd /home
tar xvfj /root/home.tar.bz2


##
# first set up ssh access to all compute nodes, so we can use ansible configuration manager to
# configure the deployment

# all hosts in openstack cloud deployment get a key pair injected for initial
# access, use this keypair to set up allowed hosts access on root for all compute
# nodes, so you must replace the following, or upload your key to /root directory
# and name it cloudkey.pem
cd /root
#cat > ${key_filename} <<EOF
#-----BEGIN RSA PRIVATE KEY-----
# you can insert your private key here, or else just copy the file
# to the controller
#-----END RSA PRIVATE KEY-----
#EOF
chmod go-rwx ${key_filename}


# create a new key pair we will use within cluster to set up
# ssh access for ansible configuration and openmpi computation
chmod 700 /root/.ssh
echo "    StrictHostKeyChecking no" >> /etc/ssh/ssh_config
ssh-keygen -t rsa -b 2048 -P "" -f /root/.ssh/id_rsa -C "Open MPI / Ansible"

# now copy the id_rsa.pub into the authorized_keys of all compute nodes
for ((node_num=1; node_num<=${node_count}; node_num++))
do
    compute_ip="192.168.0.${node_num}"
    echo "setup compute host ip: ${compute_ip}"
    scp -i ${key_filename} .ssh/id_rsa.pub ${default_user}@${compute_ip}:/home/${default_user}/mpiansiblekey.pub
    ssh -i ${key_filename} ${default_user}@${compute_ip} 'sudo su -s /bin/bash -c "cat /home/${default_user}/mpiansiblekey.pub >> /root/.ssh/authorized_keys" root'
done

# change official hosnames on all hosts to the pattern we desire
for ((node_num=1; node_num<=${node_count}; node_num++))
do
    compute_ip="192.168.0.${node_num}"
    hostname=$(printf "%s-%02d" ${base_hostname} ${node_num})
    ssh root@${compute_ip} "hostnamectl set-hostname ${hostname}"
    ssh root@${compute_ip} "sed -i 's/preserve_hostname: false/preserve_hostname: true/g' /etc/cloud/cloud.cfg"
done

# create a local /etc/hosts file for all compute nodes, so we can use names rather than
# ips for ease of configuration from this point on
config=/root/mpi-compute-hosts.txt
cat > ${config} <<EOF

# Open MPI compute cluster host names
EOF
for ((node_num=1; node_num<=${node_count}; node_num++))
do
    compute_ip="192.168.0.${node_num}"
    printf "%s %s-%02d\n" ${compute_ip} ${base_hostname} ${node_num} >> ${config}
done

for ((node_num=1; node_num<=${node_count}; node_num++))
do
    compute_ip="192.168.0.${node_num}"
    scp ${config} root@${compute_ip}:${config}
    ssh root@${compute_ip} "cat ${config} >> /etc/hosts"
done


##
# Once trusted ssh access is enabled on all hosts, get ansible and
# create playbooks to configure the compute hosts with open mpi and
# all needed tools

# Need to make sure all hosts are up to date and have a python interpreter
# installed for ansible
for ((node_num=1; node_num<=${node_count}; node_num++))
do
    compute_ip="192.168.0.${node_num}"
    ssh root@${compute_ip} "apt update"
    ssh root@${compute_ip} "DEBIAN_FRONTEND=noninteractive apt -y dist-upgrade"
    ssh root@${compute_ip} "DEBIAN_FRONTEND=noninteractive apt -y install python"
    ssh root@${compute_ip} "apt -y autoremove"
done

# Install ansible from PPA repository
DEBIAN_FRONTEND=noninteractive apt -y install software-properties-common
apt-add-repository -y ppa:ansible/ansible
apt update
DEBIAN_FRONTEND=noninteractive apt -y install ansible

# create hosts file for our openmpi cluster configuration management
config="/etc/ansible/hosts"
cp --backup=numbered ${config} ${config}.orig
cat > ${config} <<EOF
# Open MPI cluster group, all nodes in cluster
[openmpi_cluster]
EOF
for ((node_num=1; node_num<=${node_count}; node_num++))
do
    compute_ip="192.168.0.${node_num}"
    printf "%s-%02d ansible_host=%s\n" ${base_hostname} ${node_num} ${compute_ip} >> ${config}
done

cat >> ${config} <<EOF

# A group with only the head node/controller of the cluster
# this is the node where the nfs mount of home directories
# resides
[openmpi_controller]
EOF
compute_ip="192.168.0.1"
printf "%s-%02d ansible_host=%s\n" ${base_hostname} 1 ${compute_ip} >> ${config}

cat >> ${config} <<EOF

# A group with only the compute nodes of the cluster
[openmpi_nodes]
EOF
for ((node_num=2; node_num<=${node_count}; node_num++))
do
    compute_ip="192.168.0.${node_num}"
    printf "%s-%02d ansible_host=%s\n" ${base_hostname} ${node_num} ${compute_ip} >> ${config}
done

# create group variables for our openmpi-cluster ansible configuration
mkdir -p /etc/ansible/group_vars
config="/etc/ansible/group_vars/openmpi_cluster"
cat > ${config} <<EOF
---
ansible_user: root
EOF


##
# create an explicit mpi_hosts file for use by mpi commands, which will be copied
# to all hosts for use by mpi
hostfile=/root/mpi_hosts
cat > ${hostfile} <<EOF
# Open MPI compute cluster host names for --hostfile 
EOF
for ((node_num=1; node_num<=${node_count}; node_num++))
do
    printf "%s-%02d\n" ${base_hostname} ${node_num} >> ${hostfile}
done


# Install and configure Open MPI
# We have ansible configured now.  Create a playbook to do all of the tasks we need
# done to configure the defined openmpi_cluster hosts for Open MPI computations
config="/root/openmpi-compute-cluster-playbook.yml"
#cat > ${config} <<EOF
# copy the openmpi-compute-cluster-playbook.yml file to controller to run the configuration
#EOF
ansible-playbook ${config}

# Configure the NFS exports on the Host Server
config=/etc/exports
cp --backup=numbered ${config} ${config}.orig
for ((node_num=1; node_num<=${node_count}; node_num++))
do
    compute_ip="192.168.0.${node_num}"
    printf "/home\t\t%s(rw,sync,no_root_squash,no_subtree_check)\n" ${compute_ip} >> ${config}
done
systemctl restart nfs-kernel-server


# now mount the remote nfs share on the compute nodes, this hides/masks the
# existing /home directory, but that is ok for our purposes
ansible -m shell -a 'cp --backup=numbered /etc/fstab /etc/fstab.orig' openmpi_nodes
#ansible -m shell -a 'echo "${base_hostname}-controller:/home        /home        nfs  auto,nofail,noatime,nolock,intr,tcp,actimeo=1800 0 0" >> /etc/fstab' openmpi_nodes
#ansible -m shell -a 'mount ${base_hostname-controller}:/home /home' openmpi_nodes
ansible -m mount -a "src=${base_hostname}-01:/home path=/home state=mounted fstype=nfs opts='auto,nofail,noatime,nolock,intr,tcp,actimeo=1800'" openmpi_nodes

# now create a key for the ${default_user} user, and add it to known_hosts
# NOTE: since home directory is nfs mounted/shared, by copying the pub key into authorized_keys here
# we open up key based authorization for the ${default_user} user to all compute nodes
su -s /bin/bash -c 'ssh-keygen -t rsa -b 2048 -P "" -f /home/${default_user}/.ssh/id_rsa -C "Open MPI" ' ${default_user}
su -s /bin/bash -c 'cat /home/${default_user}/.ssh/id_rsa.pub >> /home/${default_user}/.ssh/authorized_keys' ${default_user}

# verify openmpi on this single host works
# do these as the ${default_user} user
# su ${default_user}
config="/home/${default_user}/test-mpi-setup.sh"
cat > ${config} <<EOF
mkdir -p /home/${default_user}/samples
cd /home/${default_user}/samples
sudo cp /root/mpi_hosts .
sudo chown ${default_user}:${default_user} /home/${default_user}/samples/mpi_hosts
wget -c https://raw.githubusercontent.com/open-mpi/ompi/master/examples/hello_c.c
mpicc hello_c.c -o hello
mpirun --mca btl_base_warn_component_unused 0 ./hello


wget -c https://raw.githubusercontent.com/open-mpi/ompi/master/examples/connectivity_c.c
mpicc connectivity_c.c -o connectivity
mpirun --mca btl_base_warn_component_unused 0 ./connectivity
mpirun --mca btl_base_warn_component_unused 0 -v -np 4 --hostfile /home/${default_user}/samples/mpi_hosts ./connectivity

EOF
chown ${default_user}:${default_user} ${config}
chmod u+x ${config}
#su -s /bin/bash -c '${config}' ${default_user}


##
# Get a more extensive set of benchmarks/tests for Open MPI.
# Here we use the NAS Parallel Benchmarks
# https://www.nas.nasa.gov/assets/npb/NPB3.4.tar.gz
config="/home/${default_user}/test-nas-benchmarks.sh"
cat > ${config} <<EOF
mkdir -p /home/${default_user}/nas
cd /home/${default_user}/nas
wget -c https://www.nas.nasa.gov/assets/npb/NPB3.4.tar.gz
wget -c https://www.nas.nasa.gov/assets/npb/NPB3.3.1.tar.gz
tar xvfz NPB3.3.1.tar.gz
cd NPB3.3.1/NPB3.3-SER
cp config/make.def.template config/make.def
make is CLASS=S
make is CLASS=W
make is CLASS=A
make is CLASS=B
make is CLASS=C
./bin/is.C.x

cd /home/${default_user}/nas
cd NPB3.3.1/NPB3.3-MPI
cp config/make.def.template config/make.def
# need to add -I/usr/lib/x86_64-linux-gnu/openmpi/include to FMPI_INC and CMPI_INC in this file
# need to add #include <string.h> to IS/is.c to prevent warning
# need to modify NPROCS in Makefile to 8 or appropriate number for mpi cluster
make is CLASS=S
make is CLASS=W
make is CLASS=A
make is CLASS=B
make is CLASS=C
mpirun --mca btl_base_warn_component_unused 0 -v -np 8 --hostfile /home/${default_user}/samples/mpi_hosts ./bin/is.C.8
EOF
chown ${default_user}:${default_user} ${config}
chmod u+x ${config}
#su -s /bin/bash -c '${config}' ${default_user}

## Some changes need a reboot to take effect here
# ansible -m reboot openmpi_nodes
