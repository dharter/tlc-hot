# configure these variables as desired for the LAMP components password access
hostname=`hostname`
fqdn=${hostname}.tamuc.edu
email='derek.harter@tamuc.edu'
install_dir='/opt'
config_dir='/etc/jupyterhub'

# update system
echo "Do usual update and upgrade to make sure system is up to date before we start..."
apt update
apt -y dist-upgrade
apt autoremove

# set timezone
echo "Configure timezone for our location"
timedatectl set-timezone America/Chicago

# get build essentials and other things
echo "Ensure basic build tools and make and git are available"
apt install -y build-essential make git python3 net-tools

# if want to use attached volume for home data, copy the default ubuntu user,
# delete their home directory, make the file system and attache to /home
echo "Create /home, make the file system and attach for use"
#cd /home
#tar cvfz /root/home-ubuntu.tgz ubuntu
#rm -rf /home/ubuntu

# create ext4 filesystem and attach and mount data volume for the server if using
#data_dev="/dev/vdb"
#mkfs.ext4 -F ${data_dev}
#echo "${data_dev}    /home    ext4    defaults 0 0" >> /etc/fstab
#mount -a

# restore the default user home directory
#cd /home
#tar xvfz /root/home-ubuntu.tgz
#rm /root/home-ubuntu.tgz

# start with an anaconda install and configure it with a group so
# we can add users and make members of the group to share a single
# anaconda python installation
# get latest anaconda 3.7 distribution for linux
echo "Get anaconda distribution installer and install..."
cd ${install_dir}
wget -c https://repo.anaconda.com/archive/Anaconda3-2020.11-Linux-x86_64.sh
bash /opt/Anaconda3-2020.11-Linux-x86_64.sh -b -p ${install_dir}/anaconda3
rm /opt/Anaconda3-2020.11-Linux-x86_64.sh

# we need to make sure root user can access the conda installation, so
# activate it by hand.  This sets the PATH environment variables in
# .bashrc as needed so that python and dist tools points to the one
# installed by anaconda
echo "Configure anaconda distribution for use by root..."
anaconda_path=${install_dir}/anaconda3
source ${anaconda_path}/bin/activate
conda init

# now we will set up a python group so we can manager
# the jupyterhub python used using this python installation
echo "Create python group, all users of jupyterhub must be members of python group..."
addgroup conda
chgrp -R conda ${anaconda_path}
chmod 770 -R ${anaconda_path}


# install R and kernels to run R code in jupyter notebooks
# not sure if it is a good idea to rely on the standard r packages
# to get the R system, but we will install from the ubuntu R-base

# This will actually add the cran servers so we can get the latest cutting edge R version.
# May want to disable this if don't need latest or having issues.
echo "Install R from cran repository to get latest version..."
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
add-apt-repository 'deb https://cloud.r-project.org/bin/linux/ubuntu bionic-cran35/'
apt update

# this actually installs the r packages we want/need for initial configuration
apt -y install r-base
apt -y install build-essential texinfo texlive-fonts-extra pandoc libcurl4-openssl-dev libssl-dev 
Rscript -e 'install.packages( c("reshape", "plyr", "dplyr", "ggplot2", "stringr", "lubridate", "tidyr", "igraph", "gtools", "devtools") )'
Rscript -e 'install.packages( c("highr", "markdown", "knitr", "rmarkdown") )'
Rscript -e 'install.packages("IRkernel")'
Rscript -e 'IRkernel::installspec(user = FALSE)'

# make sure python scientific library stack is basically there
/opt/anaconda3/bin/conda install -y numpy scipy matplotlib pandas seaborn keras tensorflow tensorflow-gpu statsmodels

# install pandoc / markdown using anaconda3
/opt/anaconda3/bin/conda install -y pandoc

# some packages we use in some classes external to python for generating figures/videos
apt install -y ffmpeg graphviz

# set up link to /etc/profile.d so standard .bashrc will setup path for all users
ln -s /opt/anaconda3/etc/profile.d/conda.sh /etc/profile.d/conda.sh


# create conda environment for JupyterHub with JupyterLab installation/run environment
# at one point we were having problems with version of nodejs.  I believe more recent versions of conda repo
# have fixed this, so that we get nodejs version we need.
#/opt/anaconda3/bin/conda create --yes --name jupyterhub -c conda-forge jupyterhub jupyterlab ipywidgets ipympl nodejs=10
/opt/anaconda3/bin/conda create --yes --name jupyterhub -c conda-forge jupyterhub jupyterlab ipywidgets ipympl nodejs

# create JupyterHub config file
mkdir -p /opt/anaconda3/envs/jupyterhub/etc/jupyterhub
cd /opt/anaconda3/envs/jupyterhub/etc/jupyterhub
/opt/anaconda3/envs/jupyterhub/bin/jupyterhub --generate-config

# change default url to go to public ip/hostname
sed -i "s|# c.JupyterHub.bind_url = 'http://:8000'|c.JupyterHub.bind_url = 'http://0.0.0.0:8000/jupyter'|g" jupyterhub_config.py

# set default config to use JupyterLab
sed -i "s|# c.Spawner.default_url = ''|c.Spawner.default_url = '/lab'|" jupyterhub_config.py

# use systemd to start JupyterHub on boot
mkdir -p /opt/anaconda3/envs/jupyterhub/etc/systemd

cat << EOF > /opt/anaconda3/envs/jupyterhub/etc/systemd/jupyterhub.service
[Unit]
Description=JupyterHub
After=syslog.target network.target

[Service]
User=root
Environment="PATH=/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/opt/anaconda3/envs/jupyterhub/bin"
ExecStart=/opt/anaconda3/envs/jupyterhub/bin/jupyterhub -f /opt/anaconda3/envs/jupyterhub/etc/jupyterhub/jupyterhub_config.py

[Install]
WantedBy=multi-user.target

EOF

# link to OS systemd directory
ln -s /opt/anaconda3/envs/jupyterhub/etc/systemd/jupyterhub.service /etc/systemd/system/jupyterhub.service


# add extra (system-wide) kernels for JupyterLab
# Anaconda3 (full scientific python stack)
#/opt/anaconda3/bin/conda create --yes --name py3gpu anaconda numpy scipy matplotlib pandas seaborn keras tensorflow statsmodels tensorflow-gpu pytorch torchvision cudatoolkit=10.1 ipykernel ipympl nodejs=10
#/opt/anaconda3/bin/conda create --yes --name py3gpu anaconda numpy scipy matplotlib pandas seaborn keras tensorflow statsmodels tensorflow-gpu tensorboard pytorch torchvision cudatoolkit=10.1 ipykernel ipympl nodejs
/opt/anaconda3/bin/conda create --yes --name py3gpu anaconda numpy scipy matplotlib pandas seaborn keras tensorflow=2.2.0 statsmodels tensorflow-gpu=2.2.0 tensorboard pytorch torchvision cudatoolkit=10.1 ipykernel ipympl jupyter nodejs=10.13.0
/opt/anaconda3/envs/py3gpu/bin/python -m ipykernel install --name 'py3gpu' --display-name "Python 3 w/ GPU libraries (py3gpu)"

# set up lab extensions
conda activate jupyterhub
/opt/anaconda3/envs/jupyterhub/bin/jupyter labextension install --no-build @jupyterlab/toc
/opt/anaconda3/envs/jupyterhub/bin/jupyter labextension install --no-build @aquirdturtle/collapsible_headings
/opt/anaconda3/envs/jupyterhub/bin/jupyter labextension install --no-build @jupyter-widgets/jupyterlab-manager
/opt/anaconda3/envs/jupyterhub/bin/jupyter labextension install --no-build jupyter-matplotlib
/opt/anaconda3/envs/jupyterhub/bin/jupyter lab clean
/opt/anaconda3/envs/jupyterhub/bin/jupyter lab build

conda activate py3gpu
/opt/anaconda3/envs/py3gpu/bin/jupyter labextension install --no-build @jupyter-widgets/jupyterlab-manager
/opt/anaconda3/envs/py3gpu/bin/jupyter labextension install --no-build jupyter-matplotlib
/opt/anaconda3/envs/py3gpu/bin/jupyter lab clean
/opt/anaconda3/envs/py3gpu/bin/jupyter lab build

# need to make the new environment kernel files be members of the conda group
chgrp -R conda /opt/anaconda3
chmod 770 -R /opt/anaconda3
chgrp -R conda /usr/local/share/jupyter/kernels
chmod 770 -R /usr/local/share/jupyter/kernels

# start JupyterHub and enable it as a service
systemctl start jupyterhub.service
systemctl enable jupyterhub.service



# example install of RStudio Server as well.
# We assume R was installed before this above in this script.
# Get the rstudio server version, this was most recent as of time
# of creating this install script.
apt install -y gdebi-core
#wget -c https://download2.rstudio.org/server/bionic/amd64/rstudio-server-1.2.1335-amd64.deb
#gdebi -n rstudio-server-1.2.1335-amd64.deb
#wget -c http://security.ubuntu.com/ubuntu/pool/main/o/openssl1.0/libssl1.0-dev_1.0.2n-1ubuntu5.6_amd64.deb
#wget -c http://security.ubuntu.com/ubuntu/pool/main/o/openssl1.0/libcrypto1.0.0-udeb_1.0.2n-1ubuntu5.6_amd64.udeb
wget https://download2.rstudio.org/server/bionic/amd64/rstudio-server-1.4.1106-amd64.deb
gdebi -n rstudio-server-1.4.1106-amd64.deb

# service is installed named rstudio-server, can stop, start and enable as normal
# the service is automatically from previous install and runs by default on port 8797
systemctl daemon-reload
systemctl enable rstudio-server

# set up https using a self-signed TLS certificate
# create the self signed certificate and private key
echo "Creating self-signed TLS certificates for https/ssl web server access..."
cert_conf="/etc/ssl/certs/${fqdn}.crt"
key_conf="/etc/ssl/private/${fqdn}.key"
ca_conf="/etc/ssl/certs/ca-certificates.crt"

# this is interactive, can we add options to answer these questions?
# 1: country name: US
# 2: state: Texas
# 3: locality (city): Commerce
# 4: organiztion: TAMUC
# 5: organization unit name: Dept CS
# 6: common name: ${fqdn} (tstlamp.tamuc.edu)
# 7: email address: ${email} first.last@tamuc.edu
openssl req -new -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -out ${cert_conf} -keyout ${key_conf} <<EOF
US
Texas
Commerce
TAMUC
Dept CS
${fqdn}
${email}
EOF

# set access permissions as needed (read only for user)
chmod 400 ${key_conf}


###################
# having some issues, lets try apache2
echo "Installing apache2 web server to provide proxy passthrough for JupyterHub and RStudio services..."
apt -y install apache2

# configure Apache
conf='/etc/apache2/apache2.conf'
cp --backup=numbered ${conf} ${conf}.orig
sed -i 's/KeepAlive Off/KeepAlive On/g' ${conf}
sed -i 's/MaxKeepAliveRequests 100/MaxKeepAliveRequests 50/g' ${conf}
sed -i 's/KeepAliveTimeout 10/KeepAliveTimeout 5/g' ${conf}

conf='/etc/apache2/mods-available/mpm_prefork.conf'
cp --backup=numbered ${conf} ${conf}.orig
sed -i 's/^[^#].*StartServers.*/\tStartServers\t\t5/g' ${conf}
sed -i 's/^[^#].*MinSpareServers.*/\tMinSpareServers\t\t3/g' ${conf}
sed -i 's/^[^#].*MaxSpareServers.*/\tMaxSpareServers\t\t40/g' ${conf}
sed -i 's/^[^#].*MaxRequestWorkers.*/\tMaxRequestWorkers\t200/g' ${conf}
sed -i 's/^[^#].*MaxConnectionsPerChild.*/\tMaxConnectionsPerChild\t10000/g' ${conf}

# disable event module and enable preforx
a2dismod mpm_event
a2enmod mpm_prefork

# restart apache to pick up config changes
systemctl restart apache2


# configure example virtual hosts
# We are simply putting in a whole working site/host configuration
# file here, which may be a bit dangerous. It will need to
# be checked carefully to determine if needs changes in
# different environments.
conf="/etc/apache2/sites-available/${fqdn}.conf"
cat > ${conf} <<EOF
<Directory /var/www/html/${fqdn}/public_html>
    Require all granted
</Directory>

<VirtualHost *:80>
    ServerName ${fqdn}
    ServerAlias www.${fqdn}

    Redirect / https://${fqdn}
</VirtualHost>


<VirtualHost *:443>
    <Proxy *>
        Allow from 127.0.0.1
    </Proxy>
	
    # set virtual host server name
    ServerAdmin ${email}
    ServerName ${fqdn}
    ServerAlias www.${fqdn}

    # configure SSL
    SSLEngine On
    SSLCertificateFile ${cert_conf}
    SSLCertificateKeyFile ${key_conf}
    SSLCACertificateFile ${ca_conf}

    <Proxy *>
        Allow from localhost
    </Proxy>

    # rstudio server proxy rules
    RedirectMatch permanent ^/rstudio$ /rstudio/
  
    RewriteEngine on
    RewriteCond %{HTTP:Connection} Upgrade [NC]
    RewriteCond %{HTTP:Upgrade} websocket [NC]
EOF
cat >> ${conf} <<"EOF"
    RewriteRule /rstudio/(.*)     ws://localhost:8787/$1  [P,L]
    RewriteRule /rstudio/(.*)     http://localhost:8787/$1 [P,L]
    ProxyPass /rstudio/ http://localhost:8787/
    ProxyPassReverse /rstudio/ http://localhost:8787/
    ProxyRequests Off

    # jupyterhub server proxy rules
    RewriteEngine on
    RewriteCond %{HTTP:Connection} Upgrade [NC]
    RewriteCond %{HTTP:Upgrade} websocket [NC]
    RewriteRule /jupyter/(.*) ws://localhost:8000/jupyter/$1 [P,L]
    RewriteRule /jupyter/(.*) http://localhost:8000/jupyter/$1 [P,L]
    ProxyPass /jupyter/ http://localhost:8000/jupyter/
    ProxyPassReverse /jupyter/  http://localhost:8000/jupyter/
    ProxyRequests Off
    
</VirtualHost>
EOF


# link virtual host file from sites-available to sites-enabled
a2enmod ssl rewrite proxy proxy_http proxy_wstunnel
a2ensite ${fqdn}

# disable default virtual host to minimize security risk
a2dissite 000-default.conf

systemctl reload apache2
systemctl restart apache2


# setup initial user accounts.
# setup passwords and ssh keys
# setup jupyterhub configuration

echo "Setup initial user accounts"
#users="ubuntu dharter singha"
users="dharter singha"
for user in ${users}
do
    echo "Configuring user: ${user}"

    useradd -m -d /home/${user} -s /bin/bash ${user}
    usermod -a -G conda ${user}
    mkdir -p /home/${user}/.ssh
    chmod go-rwx /home/${user}/.ssh
    cp /home/ubuntu/.ssh/authorized_keys /home/${user}/.ssh
    chown -R ${user}:${user} /home/${user}/.ssh
    su -s /bin/bash -c "source ${anaconda_path}/bin/activate && conda init" ${user}
    su -s /bin/bash -c "/opt/anaconda3/bin/jupyter notebook --generate-config" ${user}
    su -s /bin/bash -c "sed -i \"s|# c.NotebookApp.allow_origin = ''|c.NotebookApp.allow_origin='*'|g\" /home/${user}/.jupyter/jupyter_notebook_config.py" ${user}
done

# configure sudo access and set passwords for users here
su -s /bin/bash -c "ssh-keygen -t ed25519 -P '' -f /home/ahays/.ssh/id_ed25519 -C 'ahays' " ahays
cp /home/ahays/.ssh/id_ed25519.pub /home/ahays/.ssh/authorized_keys

# configure vmtun services
