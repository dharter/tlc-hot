#!/bin/bash
# Set up a jupyterhub server with a Python 3 scientific python stack based on conda
# package manager.  Also have the jupyterhub support R kernels as well.
# 
# This script tested on Ubuntu server 18.04 and 19.04 versions and relies
# on presence of newer systemd services and that the ssh service is on
# and running by default.
#
# Source References:
#   https://support.anaconda.com/customer/en/portal/articles/2777083-multi-user-anaconda-installation-on-linux
#   https://jupyterhub.readthedocs.io/en/stable/quickstart.html
#   https://www.datacamp.com/community/blog/jupyter-notebook-r
#   https://linuxconfig.org/rstudio-on-ubuntu-18-04-bionic-beaver-linux
#   https://www.edgarsdatalab.com/2017/11/24/setup-an-rstudio-server-in-ubuntu/
#   https://jupyterhub.readthedocs.io/en/stable/reference/config-proxy.html
#   https://www.linode.com/docs/web-servers/nginx/install-nginx-ubuntu/
#   https://support.rstudio.com/hc/en-us/articles/200552326-Running-RStudio-Server-with-a-Proxy
#
# NOTE: These packages appear that they will just fit, but maybe not, into a 10GB partition
# space.  So if want room for user data, will need to add a volume for home directories, or use
# an image with space above 10GB needed for the Python/Jupyter/R/Rstudio systems.


# configure these variables as desired for the LAMP components password access
hostname=`hostname`
fqdn=${hostname}.tamuc.edu
email='derek.harter@tamuc.edu'
install_dir='/opt'
config_dir='/etc/jupyterhub'

# update and upgrade all current packages as usual
echo "Do usual update and upgrade to make sure system is up to date before we start..."
apt update
apt -y upgrade
apt -y autoremove

# start with an anaconda install and configure it with a group so
# we can add users and make members of the group to share a single
# anaconda python installation
# get latest anaconda 3.7 distribution for linux
echo "Get anaconda distribution installer and install..."
cd ${install_dir}
wget -c https://repo.anaconda.com/archive/Anaconda3-2019.03-Linux-x86_64.sh
bash /opt/Anaconda3-2019.03-Linux-x86_64.sh -b -p ${install_dir}/anaconda3

# we need to make sure root user can access the conda installation, so
# activate it by hand.  This sets the PATH environment variables in
# .bashrc as needed so that python and dist tools points to the one
# installed by anaconda
echo "Configure anaconda distribution for use by root..."
anaconda_path=${install_dir}/anaconda3
source ${anaconda_path}/bin/activate
conda init

# now we will set up a python group so we can manager
# the jupyterhub python used using this python installation
echo "Create python group, all users of jupyterhub must be members of python group..."
groupadd python
chgrp -R python ${anaconda_path}
chmod 770 -R ${anaconda_path}

# now we will add the default ubuntu user to the group.
# in the final version, you should use regular useradd
# to create new users, and then adduser to add to the
# python group
echo "Set up default python user for python and jupyterhub..."
usermod -a -G python ubuntu

# adding the user to the group is not complete, need to do the
# commands to activate the install for the user.  So again when you
# create a new user, you will need to do these steps as well after
# adding user to the python group
su -s /bin/bash -c "source ${anaconda_path}/bin/activate && conda init" ubuntu


# install R and kernels to run R code in jupyter notebooks
# not sure if it is a good idea to rely on the standard r packages
# to get the R system, but we will install from the ubuntu R-base

# This will actually add the cran servers so we can get the latest cutting edge R version.
# May want to disable this if don't need latest or having issues.
echo "Install R from cran repository to get latest version..."
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
add-apt-repository 'deb https://cloud.r-project.org/bin/linux/ubuntu bionic-cran35/'
apt update

# this actually installs the r packages we want/need for initial configuration
apt -y install r-base
apt -y install build-essential texinfo texlive-fonts-extra pandoc libcurl4-openssl-dev libssl-dev 
Rscript -e 'install.packages( c("reshape", "plyr", "dplyr", "ggplot2", "stringr", "lubridate", "tidyr", "igraph", "gtools", "devtools") )'
Rscript -e 'install.packages( c("highr", "markdown", "knitr", "rmarkdown") )'
Rscript -e 'install.packages("IRkernel")'
Rscript -e 'IRkernel::installspec(user = FALSE)'


# now we begin installing and configuring the jupyterhub server
#conda install -y -q -c conda-forge jupyterhub
# had some bug when trying to get latest, so revert to an older version
echo "Install jupyterhub and configure the server..."
conda install -y -q -c conda-forge jupyterhub=0.9.6

# configure jupyterhub operation
mkdir ${config_dir}
conf="${config_dir}/jupyterhub_config.py"
jupyterhub --generate-config -f ${conf}
cp --backup=numbered ${conf} ${conf}.orig

# change default url to go to public ip/hostname
sed -i "s|#c.JupyterHub.bind_url = 'http://:8000'|c.JupyterHub.bind_url = 'http://0.0.0.0:8000/jupyter'|g" ${conf}


# make a systemd service for the jupyterhub server and configure it to be enabled
# on reboot
echo "Creating jupyterhub systemd service and enabling..."
cat > /etc/systemd/system/jupyterhub.service <<EOF
[Unit]
Description=Start JupyterHub server at startup
After=syslog.target network.target

[Service]
Type=simple
Environment="PATH=${anaconda_path}/bin:${anaconda_path}/condabin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin"

PIDFile=/var/run/jupyterhub.pid
ExecStart=${anaconda_path}/bin/jupyterhub --config ${conf}
EOF
cat >> /etc/systemd/system/jupyterhub.service <<"EOF"
ExecStop=/bin/kill `/bin/cat /var/run/jupyterhub.pid` && /bin/rm /var/run/jupyterhub.pid

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl enable jupyterhub
systemctl start jupyterhub


# example install of RStudio Server as well.
# We assume R was installed before this above in this script.
# Get the rstudio server version, this was most recent as of time
# of creating this install script.
apt install -y gdebi-core
wget -c https://download2.rstudio.org/server/bionic/amd64/rstudio-server-1.2.1335-amd64.deb
gdebi -n rstudio-server-1.2.1335-amd64.deb

# service is installed named rstudio-server, can stop, start and enable as normal
# the service is automatically from previous install and runs by default on port 8797
systemctl daemon-reload
systemctl enable rstudio-server


# set up https using a self-signed TLS certificate
# create the self signed certificate and private key
echo "Creating self-signed TLS certificates for https/ssl web server access..."
cert_conf="/etc/ssl/certs/${fqdn}.crt"
key_conf="/etc/ssl/private/${fqdn}.key"
ca_conf="/etc/ssl/certs/ca-certificates.crt"

# this is interactive, can we add options to answer these questions?
# 1: country name: US
# 2: state: Texas
# 3: locality (city): Commerce
# 4: organiztion: TAMUC
# 5: organization unit name: Dept CS
# 6: common name: ${fqdn} (tstlamp.tamuc.edu)
# 7: email address: ${email} first.last@tamuc.edu
openssl req -new -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -out ${cert_conf} -keyout ${key_conf} <<EOF
US
Texas
Commerce
TAMUC
Dept CS
${fqdn}
${email}
EOF

# set access permissions as needed (read only for user)
chmod 400 ${key_conf}


###################
# having some issues, lets try apache2
echo "Installing apache2 web server to provide proxy passthrough for JupyterHub and RStudio services..."
apt -y install apache2

# configure Apache
conf='/etc/apache2/apache2.conf'
cp --backup=numbered ${conf} ${conf}.orig
sed -i 's/KeepAlive Off/KeepAlive On/g' ${conf}
sed -i 's/MaxKeepAliveRequests 100/MaxKeepAliveRequests 50/g' ${conf}
sed -i 's/KeepAliveTimeout 10/KeepAliveTimeout 5/g' ${conf}

conf='/etc/apache2/mods-available/mpm_prefork.conf'
cp --backup=numbered ${conf} ${conf}.orig
sed -i 's/^[^#].*StartServers.*/\tStartServers\t\t5/g' ${conf}
sed -i 's/^[^#].*MinSpareServers.*/\tMinSpareServers\t\t3/g' ${conf}
sed -i 's/^[^#].*MaxSpareServers.*/\tMaxSpareServers\t\t40/g' ${conf}
sed -i 's/^[^#].*MaxRequestWorkers.*/\tMaxRequestWorkers\t200/g' ${conf}
sed -i 's/^[^#].*MaxConnectionsPerChild.*/\tMaxConnectionsPerChild\t10000/g' ${conf}

# disable event module and enable preforx
a2dismod mpm_event
a2enmod mpm_prefork

# restart apache to pick up config changes
systemctl restart apache2


# configure example virtual hosts
# We are simply putting in a whole working site/host configuration
# file here, which may be a bit dangerous. It will need to
# be checked carefully to determine if needs changes in
# different environments.
conf="/etc/apache2/sites-available/${fqdn}.conf"
cat > ${conf} <<EOF
<Directory /var/www/html/${fqdn}/public_html>
    Require all granted
</Directory>

<VirtualHost *:80>
    ServerName ${fqdn}
    ServerAlias www.${fqdn}

    Redirect / https://${fqdn}
</VirtualHost>


<VirtualHost *:443>
    <Proxy *>
        Allow from 127.0.0.1
    </Proxy>
	
    # set virtual host server name
    ServerAdmin ${email}
    ServerName ${fqdn}
    ServerAlias www.${fqdn}

    # configure SSL
    SSLEngine On
    SSLCertificateFile ${cert_conf}
    SSLCertificateKeyFile ${key_conf}
    SSLCACertificateFile ${ca_conf}

    <Proxy *>
        Allow from localhost
    </Proxy>

    # rstudio server proxy rules
    RedirectMatch permanent ^/rstudio$ /rstudio/
  
    RewriteEngine on
    RewriteCond %{HTTP:Connection} Upgrade [NC]
    RewriteCond %{HTTP:Upgrade} websocket [NC]
EOF
cat >> ${conf} <<"EOF"
    RewriteRule /rstudio/(.*)     ws://localhost:8787/$1  [P,L]
    RewriteRule /rstudio/(.*)     http://localhost:8787/$1 [P,L]
    ProxyPass /rstudio/ http://localhost:8787/
    ProxyPassReverse /rstudio/ http://localhost:8787/
    ProxyRequests Off

    # jupyterhub server proxy rules
    RewriteEngine on
    RewriteCond %{HTTP:Connection} Upgrade [NC]
    RewriteCond %{HTTP:Upgrade} websocket [NC]
    RewriteRule /jupyter/(.*) ws://localhost:8000/jupyter/$1 [P,L]
    RewriteRule /jupyter/(.*) http://localhost:8000/jupyter/$1 [P,L]
    ProxyPass /jupyter/ http://localhost:8000/jupyter/
    ProxyPassReverse /jupyter/  http://localhost:8000/jupyter/
    ProxyRequests Off
    
</VirtualHost>
EOF


# link virtual host file from sites-available to sites-enabled
a2enmod ssl rewrite proxy proxy_http proxy_wstunnel
a2ensite ${fqdn}

# disable default virtual host to minimize security risk
a2dissite 000-default.conf

systemctl reload apache2
systemctl restart apache2

# enable password authentication for default user so can log into
# the jupyterhub and rstudio services
echo -e "ubuntu\nubuntu" | passwd ubuntu > /dev/null 2>&1

# need to do the following for new users to use JubyterHub and RStudio servers
# 1. set up password based authentication
# 2. add user to the python group, ensure they can run python, jupyter and R
# 3. turns out we need to make a config change for the users for running jupyter
# generate a jupyter config file, and enable 
su -s /bin/bash -c "/opt/anaconda3/bin/jupyter notebook --generate-config" ubuntu
su -s /bin/bash -c "sed -i \"s|#c.NotebookApp.allow_origin = ''|c.NotebookApp.allow_origin='*'|g\" /home/ubuntu/.jupyter/jupyter_notebook_config.py" ubuntu

