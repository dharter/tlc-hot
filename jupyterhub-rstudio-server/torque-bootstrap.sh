apt install -y openssl libxml2-dev libboost-dev hwloc libhwloc-dev tcl tcl-dev tk tk-dev

# build and install from repository
# clone the repository and set it up
git clone https://github.com/adaptivecomputing/torque.git -b 6.1.2 torque-6.1.2
cd torque-6.1.2
./autogen.sh

# install torque on headnode with ssh
#./configure --enable-cgroups --enable-nvidia-gpus
#./configure --enable-cgroups --enable-gui --disable-gcc-warnings
./configure --enable-cgroups --enable-gui --disable-gcc-warnings --enable-nvidia-gpus --with-nvml-lib=/usr/local/cuda-11.0/lib64 --with-nvml-include=/usr/local/cuda-11.0/include --with-hwloc-path=/usr/local

make
make install
source /etc/profile.d/torque.sh # add /usr/local/bin and /usr/local/sbin to path for following steps


# initialize serverdb
./torque.setup root
qterm

# node database
config=/var/spool/torque/server_priv/nodes
cat > ${config} <<EOF
# torque/pbs compute nodes
gpu-01 np=4 gpus=2
EOF


# start pbs_server
systemctl enable pbs_server.service
systemctl start pbs_server.service

# install torque MOMs on compute nodes
make packages
libtool --finish /usr/local/lib

# configure compute nodes
# run torque comput node playbook to install moms and start pbs services on
# compute nodes.  The playbook has hardcoded the headnode name, so may need to edit it
#ansible-playbook ../step02-torque-compute-node-playbook.yml

apt install -y libcgroup-dev cgroup-tools
./torque-package-mom-linux-x86_64.sh --install
./torque-package-clients-linux-x86_64.sh --install
ldconfig
systemctl daemon-reload
systemctl enable pbs_mom.service
systemctl start pbs_mom.service
systemctl enable trqauthd.service
systemctl start trqauthd.service

# configure the scheduler
cp contrib/systemd/pbs_sched.service /usr/lib/systemd/system/pbs_sched.service
systemctl enable pbs_sched.service
systemctl start pbs_sched.service

systemctl restart pbs_server.service

# additional configuration of the queues
# have the queue manager auto detect the number of processors so if we forget to set correctly it does the right thing
qmgr -c 'set server auto_node_np = True'

# set default queue wall time to 7 days (7*24*60*60) seconds
qmgr -c "set queue batch resources_default.walltime=604800"

# we might want to set a resource limit on the memory size of the compute nodes in the batch system
qmgr -c "set queue batch resources_max.mem=8gb"

# some commands to verify basic things after install
# qstat -q
# qmgr -c 'p s'
# pbsnodes -a
# qstat -Qf batch
# echo "sleep 30" | qsub

# a basic test of submitting to the queue
#echo "date; sleep 30; echo 'sleeping...'; date" | qsu
