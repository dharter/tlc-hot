# update system
apt update
apt -y dist-upgrade
apt autoremove

# set timezone
timedatectl set-timezone America/Chicago

# get build essentials and other things
apt install -y build-essential make git python3 net-tools


# if want to use attached volume for home data, copy the default ubuntu user,
# delete their home directory, make the file system and attache to /home
cd /home
tar cvfz /root/home-ubuntu.tgz ubuntu
rm -rf /home/ubuntu

# create ext4 filesystem and attach and mount data volume for the server if using
data_dev="/dev/vdb"
mkfs.ext4 -F ${data_dev}
echo "${data_dev}    /home    ext4    defaults 0 0" >> /etc/fstab
#mkdir /data
mount -a

# restore the default user home directory
cd /home
tar xvfz /root/home-ubuntu.tgz
rm /root/home-ubuntu.tgz


# add group if sharing data directory
addgroup arthreat
chgrp -R arthreat /data
chmod -R g+rwx /data
chmod -R o-rwx /data

# create initial user accounts
useradd -m -d /home/dharter -s /bin/bash dharter
usermod -a -G arthreat dharter
mkdir -p /home/dharter/.ssh
chmod go-rwx /home/dharter/.ssh
cp /home/ubuntu/.ssh/authorized_keys /home/dharter/.ssh
chown -R dharter:dharter /home/dharter/.ssh
#passwd dharter

useradd -m -d /home/ahays -s /bin/bash ahays
usermod -a -G arthreat ahays
su -s /bin/bash -c "ssh-keygen -t ed25519 -P '' -f /home/ahays/.ssh/id_ed25519 -C 'ahays' " ahays
cp /home/ahays/.ssh/id_ed25519.pub /home/ahays/.ssh/authorized_keys
chown -R ahays:ahays /home/ahays/.ssh
chmod go-rwx /home/ahays/.ssh/authorized_keys
#passwd ahays


# set up vmtun service now

# set up sudo access for users we want to have su privileges
cat > /etc/sudoers.d/dharter <<EOF
# user rules for dharter
dharter ALL=(ALL) NOPASSWD:ALL
EOF

cat > /etc/sudoers.d/ahays <<EOF
# user rules for ahays
ahays ALL=(ALL) NOPASSWD:ALL
EOF

# set up nvidia gpu card and driver
apt install -y nvidia-driver-440

# need a reboot here...

# install cuda
wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-ubuntu2004.pin
mv cuda-ubuntu2004.pin /etc/apt/preferences.d/cuda-repository-pin-600
wget http://developer.download.nvidia.com/compute/cuda/11.0.2/local_installers/cuda-repo-ubuntu2004-11-0-local_11.0.2-450.51.05-1_amd64.deb
dpkg -i cuda-repo-ubuntu2004-11-0-local_11.0.2-450.51.05-1_amd64.deb
apt-key add /var/cuda-repo-ubuntu2004-11-0-local/7fa2af80.pub
apt-get update
apt-get -y install cuda

# need a reboot again

# install cuda-toolkit and cuDNN deep learning libraries
apt install -y nvidia-cuda-toolkit

# have to copy this file by hand to the guest machine
mv /home/ubuntu/cudnn-11.0-linux-x64-v8.0.2.39.tgz /root 
tar xvfz cudnn-11.0-linux-x64-v8.0.2.39.tgz
cp cuda/include/cudnn.h /usr/lib/cuda/include/
cp cuda/lib64/libcudnn* /usr/lib/cuda/lib64/
chmod a+r /usr/lib/cuda/include/cudnn.h /usr/lib/cuda/lib64/libcudnn*

# may need to ensure cuda path in dynamic library path

echo 'export LD_LIBRARY_PATH=/usr/lib/cuda/lib64:$LD_LIBRARY_PATH' >> /root/.bashrc
echo 'export LD_LIBRARY_PATH=/usr/lib/cuda/include:$LD_LIBRARY_PATH' >> /root/.bashrc

echo 'export LD_LIBRARY_PATH=/usr/lib/cuda/lib64:$LD_LIBRARY_PATH' >> /home/dharter/.bashrc
echo 'export LD_LIBRARY_PATH=/usr/lib/cuda/include:$LD_LIBRARY_PATH' >> /home/dharter/.bashrc

echo 'export LD_LIBRARY_PATH=/usr/lib/cuda/lib64:$LD_LIBRARY_PATH' >> /home/ahays/.bashrc
echo 'export LD_LIBRARY_PATH=/usr/lib/cuda/include:$LD_LIBRARY_PATH' >> /home/ahays/.bashrc

# get anaconda distribution installed
wget -c https://repo.anaconda.com/archive/Anaconda3-2020.02-Linux-x86_64.sh
bash Anaconda3-2020.02-Linux-x86_64.sh -b -p /opt/anaconda3
export PATH="/opt/anaconda3/bin:$PATH"
addgroup conda # group for people who need to use conda
chgrp -R conda /opt/anaconda3
chmod 770 -R /opt/anaconda3
usermod -a -G conda dharter
usermod -a -G conda ahays

# activate conda for users
su -s /bin/bash -c "source /opt/anaconda3/bin/activate && conda init" ubuntu
su -s /bin/bash -c "source /opt/anaconda3/bin/activate && conda init" dharter
su -s /bin/bash -c "source /opt/anaconda3/bin/activate && conda init" ahays


# make sure python scientific library stack is basically there
/opt/anaconda3/bin/conda install -y numpy scipy matplotlib pandas seaborn keras tensorflow tensorflow-gpu statsmodels

# install pandoc / markdown using anaconda3
/opt/anaconda3/bin/conda install -y pandoc

# some packages we use in some classes external to python for generating figures/videos
apt install -y ffmpeg graphviz

# set up link to /etc/profile.d so standard .bashrc will setup path for all users
ln -s /opt/anaconda3/etc/profile.d/conda.sh /etc/profile.d/conda.sh


cat << "END_ROOT" >> /root/.bashrc

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/opt/anaconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/opt/anaconda3/etc/profile.d/conda.sh" ]; then
        . "/opt/anaconda3/etc/profile.d/conda.sh"
    else
        export PATH="/opt/anaconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<
END_ROOT


cat << "END_VAGRANT" >> /home/dharter/.bashrc

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/opt/anaconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/opt/anaconda3/etc/profile.d/conda.sh" ]; then
        . "/opt/anaconda3/etc/profile.d/conda.sh"
    else
        export PATH="/opt/anaconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<
END_VAGRANT

cat << "END_VAGRANT" >> /home/mmete/.bashrc

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/opt/anaconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/opt/anaconda3/etc/profile.d/conda.sh" ]; then
        . "/opt/anaconda3/etc/profile.d/conda.sh"
    else
        export PATH="/opt/anaconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<
END_VAGRANT


# create conda environment for JupyterHub with JupyterLab installation/run environment
/opt/anaconda3/bin/conda create --yes --name jupyterhub -c conda-forge jupyterhub jupyterlab ipywidgets ipympl nodejs=10

# create JupyterHub config file
mkdir -p /opt/anaconda3/envs/jupyterhub/etc/jupyterhub
cd /opt/anaconda3/envs/jupyterhub/etc/jupyterhub
/opt/anaconda3/envs/jupyterhub/bin/jupyterhub --generate-config

# change default url to go to public ip/hostname
#sed -i "s|#c.JupyterHub.bind_url = 'http://:8000'|c.JupyterHub.bind_url = 'http://0.0.0.0:8000/jupyter'|g" jupyterhub_config.py

# set default config to use JupyterLab
sed -i "s|#c.Spawner.default_url = ''|c.Spawner.default_url = '/lab'|" jupyterhub_config.py

# use systemd to start JupyterHub on boot
mkdir -p /opt/anaconda3/envs/jupyterhub/etc/systemd

cat << EOF > /opt/anaconda3/envs/jupyterhub/etc/systemd/jupyterhub.service
[Unit]
Description=JupyterHub
After=syslog.target network.target

[Service]
User=root
Environment="PATH=/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/opt/anaconda3/envs/jupyterhub/bin"
ExecStart=/opt/anaconda3/envs/jupyterhub/bin/jupyterhub -f /opt/anaconda3/envs/jupyterhub/etc/jupyterhub/jupyterhub_config.py

[Install]
WantedBy=multi-user.target

EOF

# link to OS systemd directory
ln -s /opt/anaconda3/envs/jupyterhub/etc/systemd/jupyterhub.service /etc/systemd/system/jupyterhub.service


# add extra (system-wide) kernels for JupyterLab
# Anaconda3 (full scientific python stack)
/opt/anaconda3/bin/conda create --yes --name python3-datasci anaconda numpy scipy matplotlib pandas seaborn keras tensorflow statsmodels tensorflow-gpu pytorch torchvision cudatoolkit=10.1 ipykernel ipympl nodejs=10
/opt/anaconda3/envs/python3-datasci/bin/python -m ipykernel install --name 'python3-datasci' --display-name "python3-datasci"

# need to make the new environment kernel files be members of the conda group
chgrp -R conda /opt/anaconda3
chmod 770 -R /opt/anaconda3

# start JupyterHub and enable it as a service
systemctl start jupyterhub.service
systemctl enable jupyterhub.service

# set up lab extensions
conda activate jupyterhub
/opt/anaconda3/envs/jupyterhub/bin/jupyter labextension install --no-build @jupyterlab/toc
/opt/anaconda3/envs/jupyterhub/bin/jupyter labextension install --no-build @aquirdturtle/collapsible_headings
/opt/anaconda3/envs/python3-datasci/bin/jupyter labextension install --no-build @jupyter-widgets/jupyterlab-manager
/opt/anaconda3/envs/python3-datasci/bin/jupyter labextension install --no-build jupyter-matplotlib
/opt/anaconda3/envs/jupyterhub/bin/jupyter lab clean
/opt/anaconda3/envs/jupyterhub/bin/jupyter lab build

conda activate python3-datasci
/opt/anaconda3/envs/python3-datasci/bin/jupyter labextension install --no-build @jupyter-widgets/jupyterlab-manager
/opt/anaconda3/envs/python3-datasci/bin/jupyter labextension install --no-build jupyter-matplotlib
/opt/anaconda3/envs/python3-datasci/bin/jupyter lab clean
/opt/anaconda3/envs/python3-datasci/bin/jupyter lab build





# need to do the following for new users to use JubyterHub and RStudio servers
# 1. set up password based authentication
# 2. add user to the python group, ensure they can run python, jupyter and R
# 3. turns out we need to make a config change for the users for running jupyter
# generate a jupyter config file, and enable 
su -s /bin/bash -c "/opt/anaconda3/envs/python3-datasci/bin/jupyter notebook --generate-config" ubuntu
su -s /bin/bash -c "sed -i \"s|#c.NotebookApp.allow_origin = ''|c.NotebookApp.allow_origin='*'|g\" /home/ubuntu/.jupyter/jupyter_notebook_config.py" ubuntu

su -s /bin/bash -c "/opt/anaconda3/envs/python3-datasci/bin/jupyter notebook --generate-config" dharter
su -s /bin/bash -c "sed -i \"s|#c.NotebookApp.allow_origin = ''|c.NotebookApp.allow_origin='*'|g\" /home/dharter/.jupyter/jupyter_notebook_config.py" dharter

su -s /bin/bash -c "/opt/anaconda3/envs/python3-datasci/bin/jupyter notebook --generate-config" ahays
su -s /bin/bash -c "sed -i \"s|#c.NotebookApp.allow_origin = ''|c.NotebookApp.allow_origin='*'|g\" /home/ahays/.jupyter/jupyter_notebook_config.py" ahays

su -s /bin/bash -c "/opt/anaconda3/envs/python3-datasci/bin/jupyter notebook --generate-config" mmete
su -s /bin/bash -c "sed -i \"s|#c.NotebookApp.allow_origin = ''|c.NotebookApp.allow_origin='*'|g\" /home/mmete/.jupyter/jupyter_notebook_config.py" mmete


