#!/bin/bash
# Set up a compute instance for OpenMP shared memory parallel compute tasks.
# Install and set up OpenMp, and compilters for C,C++ and fortran.  Create
# examples of compiling and running/testing the shared memory OpenMP programs.
# 
# This script tested on Ubuntu server 18.04 and 19.04 versions and relies
# on presence of newer systemd services and that the ssh service is on
# and running by default.
#
# Source References:



# configure these variables as desired for the LAMP components password access
hostname=`hostname`
fqdn=${hostname}.tamuc.edu


# update and upgrade all current packages as usual
echo "Do usual update and upgrade to make sure system is up to date before we start..."
apt update
apt -y upgrade
apt -y autoremove


# fist install gnu software dev tools, inclding gcc, g++ gfortran and make build tools
# also install libomp-dev for OpenMP? 
apt -y install build-essential gfortran libomp-dev
# gcc --version
# g++ --version
# gfortran --version
# make --version

# as the default user, get openmp-benchmarks repository files and run the benchmarks
su -s /bin/bash -c "cd ~ && git clone https://dharter@bitbucket.org/dharter/openmp-benchmarks.git openmp" ubuntu
su -s /bin/bash -c "cd ~/openmp && make && make benchmarks" ubuntu
